<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentariosBlog extends Model
{
    protected $table = 'comentarios_blog';
    protected $fillable = ['nombre', 'email', 'comentario', 'idBlog'];
}
