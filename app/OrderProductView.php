<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProductView extends Model
{
    protected $table = 'orderProductView';
}
