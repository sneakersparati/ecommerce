<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $fillable = ['titulo', 'idCategoria', 'cuerpo', 'portada', 'idAutor', 'activo'];
}
