<?php

namespace App\Http\Controllers;

use App\Producto;
use App\ProductoValorAtributo;
use App\ProductoValorAtributoView;
use App\ProductoView;
use App\ValorAtributo;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function detail($lang)
    {
        $product = ProductoView::where('link', $lang)->where('activo', 1)->first();
        $relacionados = ProductoView::where('idMarca', $product->idMarca)->limit(20)->get();
        $tallas = ProductoValorAtributoView::where('idProducto', $product->id)->get();
        return view('producto.detail', compact('product', 'relacionados', 'tallas'));
    }

    public function collection($idCategoria, $idMarca)
    {
        $products = ProductoView::where('idCategoria', $idCategoria)->where('idMarca', $idMarca)->paginate(16);
        return view('producto.collection', compact('products'));
    }

    public function collectionIdMarca($idMarca)
    {
        $products = ProductoView::where('idMarca', $idMarca)->paginate(16);
        return view('producto.collection', compact('products'));
    }

    public function collectionIdCategoria($idCategoria)
    {
        $products = ProductoView::where('idCategoria', $idCategoria)->paginate(16);
        return view('producto.collection', compact('products'));

    }

    public function slug()
    {
        ini_set('max_execution_time', 300);
        $products = Producto::get();
        foreach ($products as $product) {
            $titulo = Str::slug($product->titulo, "-");
            $producto = Producto::find($product->id);
            $producto->link = $titulo;
            $producto->save();
        }
        return 'exito';
    }

    public function search($query)
    {
        $products  = ProductoView::search($query)->paginate(16);
        return view('producto.collection', compact('products'));
    }

    public function setTallas()
    {
        set_time_limit(0);
        $productos = Producto::get();
        for ($i = 15; $i < 34; $i++) {
            foreach ($productos as $producto) {
                ProductoValorAtributo::create(['idValorAtributo' => $i, 'idProducto' => $producto->id]);
            }
        }

    }

}
