<?php

namespace App\Http\Controllers;

use App\ProductoView;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartItems = \Cart::getContent();
        $subTotal = \Cart::getSubTotal();
        $total = \Cart::getTotal();
        return view('checkout/cart', compact('cartItems', 'subTotal', 'total'));
//        return response()->json($cartItems);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'talla' => 'required'
        ]);
        $product = ProductoView::find($request->id);
        \Cart::add(array(
            'id' => $request->id,
            'name' => $request->titulo,
            'price' => $request->precio,
            'quantity' => 1,
            'associatedModel' => $product,
            'attributes' => [
                'talla' => $request->talla
            ]
        ));
        return redirect()->route('cart.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \Cart::update($id, array(
            'quantity' => array(
                'relative' => false,
                'value' => $request->quantity
            ),
        ));

        return response()->json('Success', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        \Cart::remove($request->id);
        return back();
    }
}
