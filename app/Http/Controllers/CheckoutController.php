<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderProductView;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartItems = \Cart::getContent();
        $total = \Cart::getTotal();
        $subTotal = \Cart::getSubTotal();
        return view('checkout.checkout', compact('cartItems', 'total', 'subTotal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function factura($id)
    {
        $orden = Order::find($id);
        $productos = OrderProductView::where('orderId', $orden->id);
        $pdf = PDF::loadView('pdf.order-dinamica', compact('orden', 'productos'));
        return $pdf->download('pdf.factura');

    }

    public function facturaEstatica()
    {
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML('<style>
    .container {
        text-align: center
    }

    .left {
        float: left;
    }

    .right {
        float: right;
    }

    .datos {
        padding-top: 5em;
    }

    .center {
        padding-left: 20em;
    }

    .datos-factura {
        padding-top: 5em;
    }

    .datos-factura table {
        width: 100%;
        borden: solid 1px;
    }

    .datos-factura table thead {
        background: #E9E5E5;
        text-align: center;
    }

    .datos-factura thead th {
        padding: .3em;
    }

    .datos-factura tbody {
        text-align: center;
    }

    .datos-productos {
        padding-top: 2em;
    }

    .datos-productos table {
        width: 100%;
        borden: solid 1px;
        borden-collapse: collapse;
    }

    .datos-productos table thead {
        background: #E9E5E5;
        text-align: center;
    }

    .datos-productos thead th {
        padding: .3em;
    }

    .datos-productos tbody {
        text-align: center;
    }

    .datos-productos tbody td {
        padding: .2em;
        borden-bottom: solid 1px;
    }

    .datos-total {
        padding-top: 2em;
    }

    .datos-total table {
        borden: solid 1px;
    }

    .datos-total table th {
        background: #E9E5E5;
        padding: .5em .5em .5em 1.5em;
    }

    .datos-total table td {
        padding-left: 4em;
        padding-right: .5em;
    }


</style>
<body>
<div class="container">
    <div class="right">
        09/09/2020<br>

    </div>
    <div class="left">
        <img src="img/logo_pagina.png" alt="logo" style="width: 100px;">
    </div>
</div>
<div class="datos">
    <div class="left">
        <b>Dirección de envío</b> <br> <br>
        Calle Norte 13 MZA 759 LT 12<br>
        Col. Concepcion <br>
        Valle de Chalco<br>
        56615 <br>
        Estado de México<br>

    </div>
    <div class="center">
        <b>Datos de cliente</b> <br> <br>
        Adan González Huepalcalco <br>
        hupalcalcoadan@gmail.com <br>
        5617644101 <br>
    </div>
</div>
<div class="datos-factura">
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th>Fecha de compra</th>
            <th>Folio de la órden</th>
            <th>Forma de pago</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>08/09/20</td>
            <td>SPTE-026003</td>
            <td>Efectivo</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="datos-productos">
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th>Imagen</th>
            <th>Descripcion</th>
            <th>Precio</th>
            <th>Talla</th>
            <th>Cantidad</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><img src="https://stockx-360.imgix.net//Air-Jordan-1-Retro-High-Bio-Hack/Images/Air-Jordan-1-Retro-High-Bio-Hack/Lv2/img18.jpg?auto=format,compress&q=90&updated_at=1599068019&w=1000" alt=""
                     style="width: 50px"></td>
            <td>Jordan 1 Retro High Tokyo Bio Hack</td>
            <td>$5,510.00</td>
            <td>26.5</td>
            <td>1</td>
            <td>$5,510.00</td>
        </tr>
         <tr>
            <td><img src="https://images-sneakes.s3.us-east-2.amazonaws.com/products/94.jpg" alt=""
                     style="width: 50px"></td>
            <td>NIKE SB DUNK HIGH TRIPLE BLACK</td>
            <td>$4,518.00</td>
            <td>26.5</td>
            <td>1</td>
            <td>$4,518.00</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="datos-total">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th>Subtotal:</th>
            <td>$10,028.00</td>
        </tr>
        <tr>
            <th>Gastos de envio:</th>
            <td>$0.00</td>
        </tr>
        <tr>
            <th>Total:</th>
            <td><b>$10,028.00</b></td>
        </tr>
    </table>
</div>
</body>');
        return $pdf->stream();
    }
}
