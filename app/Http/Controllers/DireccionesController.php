<?php

namespace App\Http\Controllers;

use App\Direcciones;
use App\DireccionesView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class DireccionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $direcciones = DireccionesView::where('idUsuario', Auth::user()->id)->get();
        return view('user.direcciones', compact('direcciones'));
    }

    public function button()
    {
        $direcciones = DireccionesView::where('idUsuario', Auth::user()->id)->get();
        return response()->json($direcciones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Alert::success('Dirección', 'Dirección agregada correctamente');
        $direccion = Direcciones::create($request->all());
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $direccion = Direcciones::find($id);
        return response()->json($direccion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $direccion = Direcciones::find($request->id);
        $direccion->calle = $request->calle;
        $direccion->numeroExt = $request->numeroExt;
        $direccion->numeroInt = $request->numeroInt;
        $direccion->cp = $request->cp;
        $direccion->idColonia = $request->idColonia;
        $direccion->idUsuario = $request->idUsuario;
        $direccion->alias = $request->alias;
        $direccion->save();
        Alert::success('Dirección', 'Dirección editada correctamente');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $direccion = Direcciones::destroy($request->id);
        Alert::success('Dirección', 'Dirección eliminada correctamente');
        return redirect()->back();
    }
}
