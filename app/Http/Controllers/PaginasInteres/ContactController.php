<?php

namespace App\Http\Controllers\PaginasInteres;

use App\Contactanos;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ContactController extends Controller
{
    public function index()
    {
        return view('paginasInteres.contact');
    }

    public function save(Request $request)
    {
        Contactanos::create($request->all());
        Alert::success('Registro exitoso', 'Un agente se pondra en contacto contigo');
        return redirect()->back();
    }
}
