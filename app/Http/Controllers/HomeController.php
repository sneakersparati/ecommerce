<?php

namespace App\Http\Controllers;

use App\Banners;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }
}
