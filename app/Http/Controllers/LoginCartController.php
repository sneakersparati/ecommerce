<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginCartController extends Controller
{
    public function username()
    {
        return 'email';
    }

    public function login(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            return redirect()->route('checkout.index');
        }
        return redirect()->route('cart.index');
    }
}
