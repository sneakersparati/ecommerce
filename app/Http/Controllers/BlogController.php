<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogView;
use App\CategoriasBlog;
use App\ComentariosBlog;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class BlogController extends Controller
{
    public function index()
    {
        $postRecientes = BlogView::orderBy('created_at')->limit(10)->get();
        $categorias = CategoriasBlog::get();
        $blogs = BlogView::paginate(6);
        return view('blog.blog', compact('blogs', 'categorias', 'postRecientes'));
    }

    public function find($slug)
    {
        $postRecientes = BlogView::orderBy('created_at')->limit(10)->get();
        $categorias = CategoriasBlog::get();
        $blog = BlogView::where('slug', $slug)->first();
        $coments = ComentariosBlog::where('idBlog', $blog->id)->get();
        $numeroComentarios = ComentariosBlog::where('idBlog', $blog->id)->count();
        return view('blog.blogPost', compact('blog', 'coments', 'numeroComentarios', 'categorias', 'postRecientes'));
    }

    public function comentSave(Request $request)
    {
        Alert::success('Se envio tu comentario', 'Tu comentario ha sido enviado correctamente');
        ComentariosBlog::create($request->all());
        return redirect()->back();
    }
}
