<?php

namespace App\Http\Controllers;

use App\CpSepomex;
use Illuminate\Http\Request;

class CpSepomexController extends Controller
{
    public function getColonias($cp)
    {
        $colonias = CpSepomex::where('d_codigo', $cp)->get();
        return response()->json($colonias);
    }
}
