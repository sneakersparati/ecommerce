<?php

namespace App\Http\Controllers;

use App\ProductoView;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;

class WishController extends Controller
{
    public function show(Request $request)
    {
        if (isset($_COOKIE['favoritos'])) {
            $myItem = $_COOKIE['favoritos'];
        }
        $arreglo = json_decode($myItem);
        $products = [];
        for ($i = 0; $i < count($arreglo); $i++) {
            $producto = ProductoView::where('id', $arreglo[$i])->paginate(10);
            array_push($products, $producto);
        }

        return $products;
    }
}
