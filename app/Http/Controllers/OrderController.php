<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderProductView;
use App\ProductoView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function trackOrderIndex()
    {
        $productos = ProductoView::where('activo', 1)->orderBy('fechaSubida', 'ASC')->limit(20)->get();
        return view('order.trackOrder', compact('productos'));
    }

    public function trackOrderFind(Request $request)
    {
        $orden = Order::where('numeroFolio', $request->numeroFolio)->first();

    }

    public function misOrdenes()
    {
        $ordenes = Order::where('userId', Auth::user()->id)->get();
        return view('user.ordenes', compact('ordenes'));
    }

    public function detalleOrden($id)
    {
        $detalleOrden = OrderProductView::where('orderId', $id)->get();
        return response()->json($detalleOrden);
    }
}
