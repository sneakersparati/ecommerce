<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            Alert::success('Acceso correcto', 'Ingresaste correctamente');
            return redirect()->back();
        }
        Alert::error('Acceso incorrecto', 'Tus contraseñas son incorrectas');
        return redirect()->back();
    }

    public function logout()
    {
        Auth::logout();
        Alert::success('Se cerrro la sesion', 'Sesion cerrada correctamente');
        return redirect()->back();
    }

    public function perfil()
    {
        $usuario = User::find(Auth::user()->id);
        return view('user.perfil', compact('usuario'));
    }

    public function perfilUpdate(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->fechaNacimiento = $request->fechaNacimiento;
        $user->apellidos = $request->apellidos;
        $user->telefono = $request->telefono;
        $user->newsletter = $request->newsletter;
        $user->save();
        Alert::success('Datos actualizados', 'Tus datos se actualizaron correctamente');
        return redirect()->back();
    }
}
