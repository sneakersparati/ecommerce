<?php

namespace App\Http\Controllers;

use App\DireccionesView;
use App\Order;
use App\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use MercadoPago\Payment;
use MercadoPago\SDK;
use RealRashid\SweetAlert\Facades\Alert;

class PaymentController extends Controller
{
    public function setSecret(Request $request)
    {
        $carritoItems = \Cart::getContent();
        if (!Auth::guest()) {
            $this->validate($request, [
                'emailFactura' => 'required|email',
                'nombreFactura' => 'required|string',
                'telefonoFactura' => 'required|digits:10',
                'apellidosFactura' => 'required|string',
                'calleFactura' => 'required|string',
                'numExtFactura' => 'required|string',
                'cpFactura' => 'required|digits:5',
                'idColoniaFactura' => 'required',
                'idMetodoPago' => 'required',
            ]);
            $direccion = DireccionesView::find($request->idDireccion);
            $order = Order::create([
                'userId' => Auth::user()->id,
                'emailFactura' => Auth::user()->email,
                'nombreFactura' => Auth::user()->name,
                'telefonoFactura' => Auth::user()->telefono,
                'apellidosFactura' => Auth::user()->apellidos,
                'calleFactura' => $direccion->calle,
                'numExtFactura' => $direccion->numeroExt,
                'numIntFactura' => $direccion->numeroInt,
                'cpFactura' => $direccion->cp,
                'idColoniaFactura' => $direccion->idColonia,
                'numeroFolio' => Str::random(6),
                'idMetodoPago' => $request->payment,
                'subtotal' => $request->transaction_amount,
                'total' => $request->transaction_amount
            ]);
            foreach ($carritoItems as $carritoItem) {
                OrderProduct::create([
                    'orderId' => $order->id,
                    'productId' => $carritoItem->id,
                    'talla' => $carritoItem->attributes->talla,
                    'cantidad' => $carritoItem->quantity
                ]);
            }
            if ($request->payment == 1) {
                SDK::setAccessToken("APP_USR-2742861291663423-012221-0e43bc402bc0145fb7483ae59eeed3b8-183874612");
                $payment = new Payment();
                $payment->transaction_amount = $request->transaction_amount;
                $payment->token = $request->token;
                $payment->description = "Sneakers para ti";
                $payment->installments = $request->installments;
                $payment->payment_method_id = $request->payment_method_id;
                $payment->payer = array(
                    "email" => $order->emailFactura
                );
                $payment->save();
                if ($payment->status == 'approved') {
                    \Cart::clear();
                    Alert::success('Pago realizado con exito');
                } else {
                    Alert::error('Su pago fallo');
                }
            } elseif ($request->payment == 2) {
                SDK::setAccessToken("APP_USR-2742861291663423-012221-0e43bc402bc0145fb7483ae59eeed3b8-183874612");
                $payment = new Payment();
                $payment->transaction_amount = $request->transaction_amount;
                $payment->description = "Sneakers para ti";
                $payment->payment_method_id = "oxxo";
                $payment->payer = array(
                    "email" => $order->emailFactura
                );
                $payment->save();
                if ($payment->status == 'approved') {
                    \Cart::clear();
                    Alert::success('Pago realizado con exito');
                } else {
                    Alert::error('Su pago fallo');
                }
            }
        } else {
            $this->validate($request, [
                'emailFactura' => 'required|email',
                'nombreFactura' => 'required|string',
                'telefonoFactura' => 'required|digits:10',
                'apellidosFactura' => 'required|string',
                'calleFactura' => 'required|string',
                'numExtFactura' => 'required|string',
                'cpFactura' => 'required|digits:5',
                'idColoniaFactura' => 'required',
                'idMetodoPago' => 'required',
            ]);
            $order = Order::create([
                'emailFactura' => $request->emailFactura,
                'nombreFactura' => $request->nombreFactura,
                'telefonoFactura' => $request->telefonoFactura,
                'apellidosFactura' => $request->apellidosFactura,
                'calleFactura' => $request->calleFactura,
                'numExtFactura' => $request->numExtFactura,
                'numIntFactura' => $request->numIntFactura,
                'cpFactura' => $request->cpFactura,
                'idColoniaFactura' => $request->idColoniaFactura,
                'numeroFolio' => Str::random(6),
                'idMetodoPago' => $request->payment,
                'subtotal' => $request->transaction_amount,
                'total' => $request->transaction_amount
            ]);
            foreach ($carritoItems as $carritoItem) {
                OrderProduct::create([
                    'orderId' => $order->id,
                    'productId' => $carritoItem->id,
                    'talla' => $carritoItem->attributes->talla,
                    'cantidad' => $carritoItem->quantity
                ]);
            }
            if ($request->payment == 1) {
                SDK::setAccessToken("APP_USR-2742861291663423-012221-0e43bc402bc0145fb7483ae59eeed3b8-183874612");
                $payment = new Payment();
                $payment->transaction_amount = $request->transaction_amount;
                $payment->token = $request->token;
                $payment->description = "Sneakers para ti";
                $payment->installments = $request->installments;
                $payment->payment_method_id = $request->payment_method_id;
                $payment->payer = array(
                    "email" => $order->emailFactura
                );
                $payment->save();
                if ($payment->status == 'approved') {
                    \Cart::clear();
                    Alert::success('Pago realizado con exito');
                } else {
                    Alert::error('Su pago fallo');
                }
            } elseif ($request->payment == 2) {
                SDK::setAccessToken("APP_USR-2742861291663423-012221-0e43bc402bc0145fb7483ae59eeed3b8-183874612");
                $payment = new Payment();
                $payment->transaction_amount = $request->transaction_amount;
                $payment->description = "Sneakers para ti";
                $payment->payment_method_id = "oxxo";
                $payment->payer = array(
                    "email" => $order->emailFactura
                );
                $payment->save();
                if ($payment->status == 'approved') {
                    \Cart::clear();
                    Alert::success('Pago realizado con exito');
                } else {
                    Alert::error('Su pago fallo');
                }
            }
        }


        return redirect()->back();
    }

    public function pagoEfectivo(Request $request)
    {
        $payment = new Payment();
        $payment->transaction_amount = $request->transaction_amount;
        $payment->description = "Prueba";
        $payment->payment_method_id = "oxxo";
        $payment->payer = array(
            "email" => "sneakersparati@gmail.com"
        );
        $payment->save();
    }
}
