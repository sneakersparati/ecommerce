<?php
/**
 * Created by PhpStorm.
 * User: ultron
 * Date: 12/08/2020
 * Time: 04:26 PM
 */

namespace App\Http\Composers\HomeComposers;


use App\Banners;
use App\Blog;
use App\BlogView;
use Illuminate\View\View;

class BlogComposer
{
    public function compose(View $view)
    {
        $blogs = BlogView::get();
        $view->with(['blogs' => $blogs]);
    }
}
