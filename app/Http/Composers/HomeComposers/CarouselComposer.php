<?php
/**
 * Created by PhpStorm.
 * User: ultron
 * Date: 24/07/2020
 * Time: 07:26 PM
 */

namespace App\Http\Composers\HomeComposers;


use App\Banners;
use Illuminate\View\View;

class CarouselComposer
{
    public function compose(View $view)
    {
        $banners = Banners::where('activo', 1)->get();
        $view->with(['banners' => $banners]);
    }
}
