<?php
/**
 * Created by PhpStorm.
 * User: ultron
 * Date: 24/07/2020
 * Time: 07:42 PM
 */

namespace App\Http\Composers\HomeComposers;


use App\Producto;
use App\ProductoView;
use Illuminate\View\View;

class TopSellingComposer
{
    public function compose(View $view)
    {
        $productos = ProductoView::where('activo', 1)->limit(12)->get();
        $view->with(['productos' => $productos]);
    }
}
