<?php
/**
 * Created by PhpStorm.
 * User: ultron
 * Date: 24/07/2020
 * Time: 09:33 PM
 */

namespace App\Http\Composers\HomeComposers;


use App\ProductoView;
use Illuminate\View\View;

class FeaturedComposer
{
    public function compose(View $view)
    {
        $productos = ProductoView::where('activo', 1)->orderBy('fechaSubida','ASC')->limit(20)->get();
        $view->with(['productos' => $productos]);
    }
}
