<?php
/**
 * Created by PhpStorm.
 * User: ultron
 * Date: 12/08/2020
 * Time: 02:41 AM
 */

namespace App\Http\Composers\CheckoutComposers;


use App\DireccionesView;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CheckoutComposer
{
    public function compose(View $view)
    {
        if (!Auth::guest()) {
            $direcciones = DireccionesView::where('idUsuario', Auth::user()->id)->get();
            $view->with(['direcciones' => $direcciones]);
        }
    }
}
