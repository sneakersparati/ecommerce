<?php
/**
 * Created by PhpStorm.
 * User: ultron
 * Date: 02/08/2020
 * Time: 04:08 PM
 */

namespace App\Http\Composers\LayoutComposers;


use App\Categoria;
use Illuminate\View\View;

class FooterComposer
{
    public function compose(View $view)
    {
        $categorias = Categoria::where('idPadre', 0)->get();
        $view->with(['categorias' => $categorias]);
    }
}
