<?php
/**
 * Created by PhpStorm.
 * User: ultron
 * Date: 29/07/2020
 * Time: 02:55 AM
 */

namespace App\Http\Composers\LayoutComposers;


use App\Categoria;
use App\Marcas;
use Illuminate\View\View;

class HeaderComposer
{
    public function compose(View $view)
    {
        $cartItems = \Cart::getContent();
        $total = \Cart::getTotal();
        $cartTotalQuantity = \Cart::getTotalQuantity();
        $categorias = Categoria::where('idPadre', 0)->get();
        $marcas = Marcas::where('activo', 1)->get();
        $subCategorias = Categoria::where('idPadre', '!=', 0)->get();
        $view->with(['cartItems' => $cartItems, 'total' => $total, 'cartTotalQuantity' => $cartTotalQuantity,
            'categorias' => $categorias, 'marcas' => $marcas, 'subCategorias' => $subCategorias]);
    }
}
