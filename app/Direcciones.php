<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direcciones extends Model
{
    protected $table = 'direcciones';
    protected $fillable = ['calle', 'numeroExt', 'numeroInt', 'cp', 'idColonia', 'idUsuario', 'alias'];
}
