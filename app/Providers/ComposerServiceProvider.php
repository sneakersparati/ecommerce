<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['home.carousel'], 'App\Http\Composers\HomeComposers\CarouselComposer');
        View::composer(['home.top_selling'], 'App\Http\Composers\HomeComposers\TopSellingComposer');
        View::composer(['home.featured'], 'App\Http\Composers\HomeComposers\FeaturedComposer');
        View::composer(['home.latest_news'], 'App\Http\Composers\HomeComposers\BlogComposer');
        View::composer(['layout.header'], 'App\Http\Composers\LayoutComposers\HeaderComposer');
        View::composer(['layout.footer'], 'App\Http\Composers\LayoutComposers\FooterComposer');
        View::composer(['checkout.checkout'], 'App\Http\Composers\CheckoutComposers\CheckoutComposer');
    }
}
