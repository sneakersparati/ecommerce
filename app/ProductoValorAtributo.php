<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoValorAtributo extends Model
{
    protected $table = 'producto_valor_atributo';
    protected $fillable = ['idValorAtributo', 'idProducto'];
    public $timestamps = false;
}
