<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValorAtributo extends Model
{
    protected $table = 'valor_atributo';
    protected $fillable = ['descripcion', 'activo', 'idAtributo'];
    public $timestamps = false;
}
