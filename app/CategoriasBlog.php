<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriasBlog extends Model
{
    protected $table = 'categorias_blogs';
    protected $fillable = ['categoria', 'activo'];
    public $timestamps = false;
}
