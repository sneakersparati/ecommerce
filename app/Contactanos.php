<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactanos extends Model
{
    protected $table = 'contactanos';
    protected $fillable = ['nombre', 'email', 'telefono', 'mensaje'];
}
