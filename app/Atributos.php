<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atributos extends Model
{
    protected $table = 'atributos';
    protected $fillable = ['descripcion', 'activo'];
    public $timestamps = false;
}
