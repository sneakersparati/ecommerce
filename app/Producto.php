<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Producto extends Model
{
    use Searchable;
    protected $table = 'producto';
    protected $fillable = ['titulo', 'descripcion', 'precio', 'link'];
    public $timestamps = false;

    public function imagenes()
    {
        return $this->hasMany('App\ImagenesProducto', 'idProducto');
    }

    public function formatoPrecio()
    {
        return '$' . number_format($this->precio, 2);
    }

    public function toSearchableArray()
    {
        return [
            'titulo' => $this->titulo,
            'descripcion' => $this->descripcion,
            'precio' => $this->precio,
            'link' => $this->link
        ];
    }
}
