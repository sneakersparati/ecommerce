<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'ordersProducts';
    protected $fillable = ['orderId', 'productId', 'cantidad', 'talla'];
}
