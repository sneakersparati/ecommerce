<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class ProductoView extends Model
{
    use Searchable;
    protected $table = 'productoView';

    public function formatoPrecio()
    {
        return '$' . number_format($this->precio, 2);
    }

    public function imagenes()
    {
        return $this->hasMany('App\ImagenesProducto', 'idProducto');
    }

    public function atributos()
    {
        return $this->hasMany('App\ProductoValorAtributo', 'idProducto');
    }

    public function toSearchableArray()
    {
        return [
            'titulo' => $this->titulo,
            'descripcion' => $this->descripcion,
            'precio' => $this->precio,
            'link' => $this->link,
            'url' => $this->url
        ];
    }
}
