<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    protected $table = 'banners';
    protected $fillable = ['urlBanner', 'title', 'urlSitio', 'activo', 'descripcion', 'meta'];
    public $timestamps = false;
}
