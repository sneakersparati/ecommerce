<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenesProducto extends Model
{
    protected $table = 'imagenes_productos';
    protected $fillable = ['meta', 'url', 'idProducto', 'primary'];
    public $timestamps = false;
}
