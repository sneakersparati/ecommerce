<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['userId', 'emailFactura', 'nombreFactura', 'telefonoFactura', 'apellidosFactura', 'calleFactura',
        'numExtFactura', 'numIntFactura', 'cpFactura', 'idColoniaFactura', 'numeroFolio', 'idMetodoPago', 'subtotal',
        'total', 'descuento', 'cuponDescuento'];
}
