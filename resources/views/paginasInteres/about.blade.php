@extends('index')
@section('styles')
    <link href="{{asset('css/about.css')}}" rel="stylesheet">
@endsection
@section('class-body')
    class="bg_gray"
@endsection
@section('content')
    <div class="top_banner general">
        <div class="opacity-mask d-flex align-items-md-center" data-opacity-mask="rgba(0, 0, 0, 0.1)">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-lg-8 col-md-6 text-right">
                        <h1>Dolor docendi fuisset ad<br>movet mucius diceret et qui</h1>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{asset('img/top_about.jpg')}}" class="img-fluid" alt="">
    </div>
    <!--/top_banner-->

    <div class="bg_white">
        <div class="container margin_90_0">
            <div class="row justify-content-between align-items-center content_general_row">
                <div class="col-lg-12 col-md-12">
                    <h2>¿Quienes somos?</h2>
                    <p class="mt-3">
                        En sneakers para ti, estamos comprometidos con darte el mejor servicio y calidad, contamos con
                        los pares de sneakers mas exclusivos del mercado, al mejor precio.
                    </p>
                    <p>
                        Con mas de 4 años de experiencia, consolidandonos como una empresa lider en el
                        mercado de los sneakers, dando seguridad y confianza a todos nuestros compradores, asegurando
                        siempre tu satisfacción.
                    </p>
                </div>
            </div>
        </div>
        <!--/container-->

    </div>
    <div class="bg_white">
        <div class="container margin_60_35">
            <div class="main_title">
                <h2>Porque elegir Sneakers para ti</h2>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="box_feat">
                        <i class="ti-medall-alt"></i>
                        <h3>+ 1000 Clientes</h3>
                        <p>Nuestros mas de 1000 clientes nos respaldan.</p> <br>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="box_feat">
                        <i class="ti-help-alt"></i>
                        <h3>Soporte 24 H</h3>
                        <p>Soporte 24 horas a traves del sitio web. </p> <br>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="box_feat">
                        <i class="ti-gift"></i>
                        <h3>Grandes ofertas y promociones</h3>
                        <p>Encontraras grnades ofertas y promociones en nuestro sitio web.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="box_feat">
                        <i class="ti-headphone-alt"></i>
                        <h3>Linea de atención directa</h3>
                        <p>Encontraras una linea de atención directa. </p> <br>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="box_feat">
                        <i class="ti-wallet"></i>
                        <h3>Pagos seguros</h3>
                        <p>Tus pagos son 100% seguros a traves de nuestro sitio web.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="box_feat">
                        <i class="ti-comments"></i>
                        <h3>Soporte vía chat</h3>
                        <p>Nuestro soporte via chat siempre esta disponible para tí. </p>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>
    </div>
@endsection
