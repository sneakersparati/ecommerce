@extends('index')
@section('styles')
    <link href="{{asset('css/contact.css')}}" rel="stylesheet">
@endsection
@section('class-body')
    class="bg_gray"
@endsection
@section('content')
    <div class="container margin_60">
        <div class="main_title">
            <h2>Contactanos</h2>
            <p>A continuación, encontraras nuestra información de contacto.</p>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="box_contacts">
                    <i class="ti-support"></i>
                    <h2>¿Necesitas ayuda para realizar tu compra?</h2>
                    <a href="https://api.whatsapp.com/send?phone=525530575652">55-30-57-56-52</a> - <a
                            href="mailto:ventas@sneakersparati.com.mx">ventas@sneakersparati.com.mx</a>
                    <small>Lunes a Domingo 10 am a 8 pm</small>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="box_contacts">
                    <i class="ti-package"></i>
                    <h2>Linea de atención a clientes</h2>
                    <a href="https://api.whatsapp.com/send?phone=525530575652">55-30-57-56-52</a> - <a
                            href="mailto:ventas@sneakersparati.com.mx">ventas@sneakersparati.com.mx</a>
                    <small>Lunes a Domingo 10 am a 8 pm</small>
                </div>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
    <div class="bg_white">
        <div class="container margin_60_35">
            <h4 class="pb-3">Dejanos un mensaje y un asesor te contactara</h4>
            <div class="row">
                <div class="col-lg-12 col-md-12 add_bottom_25">
                    <form action="{{route('contactanos.save')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Nombre *" name="nombre">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="email" placeholder="Email *" name="email">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Telefono *" name="telefono">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" style="height: 150px;" placeholder="Mensaje *"
                                      name="mensaje"></textarea>
                        </div>
                        <div class="form-group">
                            <input class="btn_1 full-width" type="submit" value="Enviar">
                        </div>
                    </form>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
@endsection
