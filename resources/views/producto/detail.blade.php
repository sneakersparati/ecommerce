@extends('index')
@section('styles')
    <link href="{{asset('css/product_page.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="container margin_30">
        {{--<div class="countdown_inner">-20% This offer ends in--}}
        {{--<div data-countdown="2019/05/15" class="countdown"></div>--}}
        {{--</div>--}}
        <div class="row">
            <div class="col-md-6">
                <div class="all">
                    <div class="slider">
                        <div class="owl-carousel owl-theme main">
                            @foreach($product->imagenes as $imagenes)
                                <div style="background-image: url({{asset($imagenes->url)}});"
                                     class="item-box"></div>
                            @endforeach
                        </div>
                        <div class="left nonl"><i class="ti-angle-left"></i></div>
                        <div class="right"><i class="ti-angle-right"></i></div>
                    </div>
                    <div class="slider-two">
                        <div class="owl-carousel owl-theme thumbs">
                            @foreach($product->imagenes as $imagenes)
                                <div style="background-image: url({{asset($imagenes->url)}});"
                                     class="item active"></div>
                            @endforeach
                        </div>
                        <div class="left-t nonl-t"></div>
                        <div class="right-t"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            {{--<div class="breadcrumbs">--}}
            {{--<ul>--}}
            {{--<li><a href="#">Home</a></li>--}}
            {{--<li><a href="#">Category</a></li>--}}
            {{--<li>Page active</li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            <!-- /page_header -->
                <div class="prod_info">
                    <h1>{{$product->titulo}}</h1>
                    {{--<span class="rating"><i class="icon-star voted"></i><i class="icon-star voted"></i><i--}}
                    {{--class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star"></i><em>4 reviews</em></span>--}}
                    {{--<p>--}}
                    {{--<small>SKU: MTKRY-001</small>--}}
                    {{--<br>Sed ex labitur adolescens scriptorem. Te saepe verear tibique sed. Et wisi ridens vix, lorem--}}
                    {{--iudico blandit mel cu. Ex vel sint zril oportere, amet wisi aperiri te cum.--}}
                    {{--</p>--}}
                    <div class="prod_options">
                        {{--<div class="row">--}}
                        {{--<label class="col-xl-5 col-lg-5  col-md-6 col-6 pt-0"><strong>Color</strong></label>--}}
                        {{--<div class="col-xl-4 col-lg-5 col-md-6 col-6 colors">--}}
                        {{--<ul>--}}
                        {{--<li><a href="#0" class="color color_1 active"></a></li>--}}
                        {{--<li><a href="#0" class="color color_2"></a></li>--}}
                        {{--<li><a href="#0" class="color color_3"></a></li>--}}
                        {{--<li><a href="#0" class="color color_4"></a></li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="row">
                            <label class="col-xl-5 col-lg-5 col-md-6 col-6"><strong>Talla</strong> - Guia de tallas <a
                                        href="#0" data-toggle="modal" data-target="#size-modal"><i
                                            class="ti-help-alt"></i></a></label>
                            <div class="col-xl-4 col-lg-5 col-md-6 col-6">
                                <div class="custom-select-form">
                                    <select class="wide" id="talla">
                                        <option value="" selected disabled hidden>Seleccione una talla</option>
                                        @foreach($tallas as $talla)
                                            <option value="{{$talla->valorAtributo}}">{{$talla->valorAtributo}}</option>
                                        @endforeach
                                    </select>
                                    @error('talla')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-xl-5 col-lg-5  col-md-6 col-6"><strong>Cantidad</strong></label>
                            <div class="col-xl-4 col-lg-5 col-md-6 col-6">
                                <div class="numbers-row">
                                    <input type="text" value="1" id="quantity_1" class="qty2" name="quantity_1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-6">
                            <div class="price_main">
                                <span class="new_price">{{$product->formatoPrecio()}}</span>
                                {{--<span class="percentage">-20%</span>--}}
                                {{--<span class="old_price">$160.00</span>--}}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <form action="{{route('cart.store')}}" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="{{$product->id}}">
                                <input type="hidden" name="titulo" value="{{$product->titulo}}">
                                <input type="hidden" name="precio" value="{{$product->precio}}">
                                <input type="hidden" name="talla" id="talla-selected">
                                <div class="btn_add_to_cart">
                                    <button class="btn_1" type="submit">Añadir a carrito</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /prod_info -->
                <div class="product_actions">
                    <ul>
                        <li>
                            <a href="#"><i class="ti-heart"></i><span>Añadir a lista de deseos</span></a>
                        </li>
                        {{--<li>--}}
                        {{--<a href="#"><i class="ti-control-shuffle"></i><span>Comparar</span></a>--}}
                        {{--</li>--}}
                    </ul>
                </div>
                <!-- /product_actions -->
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->

    <div class="tabs_product">
        <div class="container">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a id="tab-A" href="#pane-A" class="nav-link active" data-toggle="tab" role="tab">Descripción</a>
                </li>
                {{--<li class="nav-item">--}}
                {{--<a id="tab-B" href="#pane-B" class="nav-link" data-toggle="tab" role="tab">Reviews</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
    <!-- /tabs_product -->
    <div class="tab_content_wrapper">
        <div class="container">
            <div class="tab-content" role="tablist">
                <div id="pane-A" class="card tab-pane fade active show" role="tabpanel" aria-labelledby="tab-A">
                    <div class="card-header" role="tab" id="heading-A">
                        <h5 class="mb-0">
                            <a class="collapsed" data-toggle="collapse" href="#collapse-A" aria-expanded="false"
                               aria-controls="collapse-A">
                                Descripción
                            </a>
                        </h5>
                    </div>
                    <div id="collapse-A" class="collapse" role="tabpanel" aria-labelledby="heading-A">
                        <div class="card-body">
                            <div class="row justify-content-between">
                                <div class="col-lg-6">
                                    <h3>Detalles</h3>
                                    <p>{{$product->descripcion}}</p>
                                </div>
                                {{--<div class="col-lg-5">--}}
                                {{--<h3>Specifications</h3>--}}
                                {{--<div class="table-responsive">--}}
                                {{--<table class="table table-sm table-striped">--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                {{--<td><strong>Color</strong></td>--}}
                                {{--<td>Blue, Purple</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td><strong>Size</strong></td>--}}
                                {{--<td>150x100x100</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td><strong>Weight</strong></td>--}}
                                {{--<td>0.6kg</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td><strong>Manifacturer</strong></td>--}}
                                {{--<td>Manifacturer</td>--}}
                                {{--</tr>--}}
                                {{--</tbody>--}}
                                {{--</table>--}}
                                {{--</div>--}}
                                {{--<!-- /table-responsive -->--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /TAB A -->
                <!-- /tab B -->
            </div>
            <!-- /tab-content -->
        </div>
        <!-- /container -->
    </div>
    <!-- /tab_content_wrapper -->

    <div class="container margin_60_35">
        <div class="main_title">
            <h2>Relacionados</h2>
            <span>Productos</span>
            <p>Los siguientes productos te pueden interesar.</p>
        </div>
        <div class="owl-carousel owl-theme products_carousel">
            @foreach($relacionados as $relacionado)
                <div class="item">
                    <div class="grid_item product__item">
                        {{--<span class="ribbon new">New</span>--}}
                        <figure class="product__item__pic set-bg">
                            <a href="{{route('product.detail',$relacionado->link)}}">
                                <img class="owl-lazy" src="{{asset($relacionado->url)}}"
                                     data-src="{{asset($relacionado->url)}}" alt="">
                            </a>
                        </figure>
                        {{--<div class="rating"><i class="icon-star voted"></i><i class="icon-star voted"></i><i--}}
                        {{--class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star"></i>--}}
                        {{--</div>--}}
                        <div class="product__item__text">
                            <h6 class="text-left">{{$relacionado->titulo}}</h6>
                            <a href="{{route('product.detail',$relacionado->link)}}" class="add-cart">+ Ver detalle</a>
                            <h5 class="text-left">{{$relacionado->formatoPrecio()}}</h5>
                        </div>
                        <ul>
                            <li><a href="#0" class="tooltip-1" data-toggle="tooltip" data-placement="left"
                                   title="Añadir a favoritos"><i
                                            class="ti-heart"></i><span>Añadir a favoritos</span></a>
                            </li>
                            <li>
                                <form action="{{route('cart.store')}}" method="POST">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$relacionado->id}}">
                                    <input type="hidden" name="titulo" value="{{$relacionado->titulo}}">
                                    <input type="hidden" name="precio" value="{{$relacionado->precio}}">
                                    <input type="hidden" name="talla" value="26">
                                    <button class="tooltip-1" data-toggle="tooltip" data-placement="left"
                                            title="Añadir a carrito" type="submit">
                                        <i class="ti-shopping-cart"></i>
                                        <span>Añadir a carrito</span>
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <!-- /grid_item -->
                </div>
            @endforeach
        </div>
        <!-- /products_carousel -->
    </div>
    <!-- /container -->

    <!-- <div class="feat">
        <div class="container">
            <ul>
                <li>
                    <div class="box">
                        <i class="ti-gift"></i>
                        <div class="justify-content-center">
                            <h3>Envio gratis</h3>
                            <p>A toda la republica mexicana</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="box">
                        <i class="ti-wallet"></i>
                        <div class="justify-content-center">
                            <h3>Pago seguro</h3>
                            <p>Pago 100% seguro</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="box">
                        <i class="ti-headphone-alt"></i>
                        <div class="justify-content-center">
                            <h3>Soporte 24/7</h3>
                            <p>Soporte en linea las 24 horas</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div> -->
@endsection
@section('scripts')
    <script src="{{asset('js/carousel_with_thumbs.js')}}"></script>
    <script>
        $('#talla').on('change', function (e) {
            var talla = e.target.value;
            $('#talla-selected').val(talla);
        })
    </script>
@endsection
