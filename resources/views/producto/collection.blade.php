@extends('index')
@section('styles')
    <link href="{{asset('css/listing.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="container margin_30">
        <div id="stick_here"></div>
        <div class="toolbox elemento_stick version_2">
            <div class="container">
                <ul class="clearfix">
                    <li>
                        <div class="sort_select">
                            <select name="sort" id="sort">
                                <option value="price">Ordenar por precio: más bajo a más alto</option>
                                <option value="price-desc">Ordenar por precio: más alto a más bajo</option>
                            </select>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /toolbox -->
        <div class="row small-gutters">
            @foreach($products as $product)
                <div class="col-6 col-md-4 col-xl-3">
                    <div class="grid_item product__item">
                        <figure class="product__item__pic set-bg">
                            {{--<span class="ribbon off">-30%</span>--}}
                            <a href="{{route('product.detail',$product->link)}}">
                                <img class="img-fluid lazy" src="{{asset($product->url)}}"
                                     data-src="{{asset($product->url)}}" alt="">
                            </a>
                            {{--<div data-countdown="2019/05/15" class="countdown"></div>--}}
                        </figure>
                        <div class="product__item__text">
                            <h6 class="text-left">{{$product->titulo}}</h6>
                            <a href="{{route('product.detail',$product->link)}}">
                                + Ver detalle
                            </a>
                            <div class="price_box">
                                <h5 class="text-left">{{$product->formatoPrecio()}}</h5>
                                {{--<span class="old_price">$60.00</span>--}}
                            </div>
                        </div>
                        <ul>
                            <li><button class="tooltip-1" data-toggle="tooltip" data-placement="left"
                                   title="Añadir a favoritos" id="heart"><i
                                            class="ti-heart"></i><span>Añadir a favoritos</span></button>
                            </li>
                            <li>
                                <form action="{{route('cart.store')}}" method="POST">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$product->id}}">
                                    <input type="hidden" name="titulo" value="{{$product->titulo}}">
                                    <input type="hidden" name="precio" value="{{$product->precio}}">
                                    <input type="hidden" name="talla" value="26">
                                    <button class="tooltip-1" data-toggle="tooltip" data-placement="left"
                                            title="Añadir a carrito" type="submit">
                                        <i class="ti-shopping-cart"></i>
                                        <span>Añadir a carrito</span>
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <!-- /grid_item -->
                </div>
        @endforeach
        <!-- /col -->
        </div>
        <!-- /row -->

        <div class="pagination__wrapper">
            <ul class="pagination">
                {{$products->links()}}
            </ul>
        </div>

    </div>

    <script>
        $('#heart').on("click", function () {
            alert('click a corazon');
        })
    </script>
@endsection
