<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Ansonika">
    <title>Sneakers para ti</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/apple-touch-icon-57x57-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72"
          href="{{asset('img/apple-touch-icon-72x72-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
          href="{{asset('img/apple-touch-icon-114x114-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144"
          href="{{asset('img/apple-touch-icon-144x144-precomposed.png')}}">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap" rel="stylesheet">

    <!-- LINEAR ICONS -->
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css" />

    <!-- BASE CSS -->
    <link href="{{asset('css/bootstrap.custom.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

    <!-- SPECIFIC CSS -->
    <link href="{{asset('css/home_1.css')}}" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/css-fadeshow-master/css-fadeshow.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
    @yield('styles')
</head>

<body>

<div id="page">

    @include('layout.header')

    <main @yield('classBody')>
        @yield('content')
        @include('sweetalert::alert')
    </main>
@include('layout.footer')
<!--/footer-->
</div>
<!-- page -->

<div id="toTop"></div><!-- Back to top button -->

<!-- COMMON SCRIPTS -->
<script src="{{asset('js/common_scripts.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>

@yield('scripts')
<script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
<script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
<script>
    (function () {
        var client = algoliasearch('2IK782JNU9', 'acf5cf71e7f8b99254d5e4228e89044d');
        var index = client.initIndex('productoView');
        var enterPressed = false;
        //initialize autocomplete on search input (ID selector must match)
        autocomplete('#searchInput',
            {hint: false}, {
                source: autocomplete.sources.hits(index, {hitsPerPage: 10}),
                //value to be displayed in input control after user's suggestion selection
                displayKey: 'titulo',
                //hash of templates used when rendering dataset
                templates: {
                    //'suggestion' templating function used to render a single suggestion
                    suggestion: function (suggestion) {
                        const markup = `
                        <div class="algolia-result">
                            <span>
                            <img src="${suggestion.url}" alt="img" class="algolia-thumb">
                                ${suggestion._highlightResult.titulo.value}
                            </span>
                            <span>$${(suggestion.precio).toFixed(2)}</span>
                        </div>

                    `;

                        return markup;
                    },
                    empty: function (result) {
                        return 'Sorry, we did not find any results for "' + result.query + '"';
                    }
                }
            }).on('autocomplete:selected', function (event, suggestion, dataset) {
            window.location.href = window.location.origin + '/product/detail/' + suggestion.link;
            enterPressed = true;
        }).on('keyup', function (event) {
            if (event.keyCode == 13 && !enterPressed) {
                window.location.href = window.location.origin + '/product/search/' + document.getElementById('searchInput').value;
            }
        });
    })();
    (function () {
        var client = algoliasearch('2IK782JNU9', 'acf5cf71e7f8b99254d5e4228e89044d');
        var index = client.initIndex('productoView');
        var enterPressed = false;
        //initialize autocomplete on search input (ID selector must match)
        autocomplete('#searchInput2',
            {hint: false}, {
                source: autocomplete.sources.hits(index, {hitsPerPage: 10}),
                //value to be displayed in input control after user's suggestion selection
                displayKey: 'titulo',
                //hash of templates used when rendering dataset
                templates: {
                    //'suggestion' templating function used to render a single suggestion
                    suggestion: function (suggestion) {
                        const markup = `
                        <div class="algolia-result">
                            <span>
                            <img src="${suggestion.url}" alt="img" class="algolia-thumb">
                                ${suggestion._highlightResult.titulo.value}
                            </span>
                            <span>$${(suggestion.precio).toFixed(2)}</span>
                        </div>

                    `;

                        return markup;
                    },
                    empty: function (result) {
                        return 'Sorry, we did not find any results for "' + result.query + '"';
                    }
                }
            }).on('autocomplete:selected', function (event, suggestion, dataset) {
            window.location.href = window.location.origin + '/product/detail/' + suggestion.link;
            enterPressed = true;
        }).on('keyup', function (event) {
            if (event.keyCode == 13 && !enterPressed) {
                window.location.href = window.location.origin + '/product/search/' + document.getElementById('searchInput2').value;
            }
        });
    })();

</script>
</body>

</html>
