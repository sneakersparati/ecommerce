@extends('index')
@section('styles')
    <link href="{{asset('css/blog.css')}}" rel="stylesheet">
@endsection
@section('classBody')
    class="bg_gray"
@endsection
@section('content')
    <div class="container margin_30">
        <div class="page_header">
            <h1>Blog y ultimas noticas de Sneakers para ti</h1>
        </div>
        <!-- /page_header -->
        <div class="row">
            <div class="col-lg-9">
                <div class="widget search_blog d-block d-sm-block d-md-block d-lg-none">
                    <div class="form-group">
                        <input type="text" name="search" id="search" class="form-control" placeholder="Buscar..">
                        <button type="submit"><i class="ti-search"></i><span class="sr-only">Buscar</span></button>
                    </div>
                </div>
                <!-- /widget -->
                <div class="row">
                    @foreach($blogs as $blog)
                        <div class="col-md-6">
                            <article class="blog">
                                <figure>
                                    <a href="{{route('blog.find',$blog->slug)}}"><img src="{{asset($blog->portada)}}"
                                                                                      alt=""
                                                                                      class="img-fluid" height="auto"
                                                                                      width="auto">
                                        <div class="preview"><span>Leer mas</span></div>
                                    </a>
                                </figure>
                                <div class="post_info">
                                    <small>{{$blog->categoria}} - {{$blog->created_at}}</small>
                                    <h2><a href="{{route('blog.find',$blog->slug)}}">{{$blog->titulo}}</a></h2>
                                    <p>{{Str::limit($blog->cuerpo, 100)}}    </p>
                                    <ul>
                                        <li>
                                            <div class="thumb"><img src="img/avatar.jpg" alt=""></div>
                                            {{$blog->autor}}
                                        </li>
                                        <li><i class="ti-comment"></i>20</li>
                                    </ul>
                                </div>
                            </article>
                            <!-- /article -->
                        </div>
                    @endforeach
                </div>
                <!-- /row -->

                <div class="pagination__wrapper no_border add_bottom_30">
                    <ul class="pagination">
                        {{$blogs->links()}}
                    </ul>
                </div>
                <!-- /pagination -->

            </div>
            <!-- /col -->

            <aside class="col-lg-3">
                <div class="widget search_blog d-none d-sm-none d-md-none d-lg-block">
                    <div class="form-group">
                        <input type="text" name="search" id="search_blog" class="form-control" placeholder="Buscar..">
                        <button type="submit"><i class="ti-search"></i><span class="sr-only">Buscar</span></button>
                    </div>
                </div>
                <!-- /widget -->
                <div class="widget">
                    <div class="widget-title">
                        <h4>Post mas recientes</h4>
                    </div>
                    <ul class="comments-list">
                        @foreach($postRecientes as $postReciente)
                            <li>
                                <div class="alignleft">
                                    <a href="{{route('blog.find',$postReciente->slug)}}"><img src="{{$postReciente->portada}}" alt="" class="img-fluid"></a>
                                </div>
                                <small>{{$postReciente->categoria}} - {{$postReciente->created_at}}</small>
                                <h3><a href="{{route('blog.find',$postReciente->slug)}}" title="">{{$postReciente->titulo}}</a></h3>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /widget -->
                <div class="widget">
                    <div class="widget-title">
                        <h4>Categorias</h4>
                    </div>
                    <ul class="cats">
                        @foreach($categorias as $categoria)
                            <li><a href="#">{{$categoria->categoria}} <span>(12)</span></a></li>
                        @endforeach
                    </ul>
                </div>
                <!-- /widget -->
                {{--<div class="widget">--}}
                    {{--<div class="widget-title">--}}
                        {{--<h4>Popular Tags</h4>--}}
                    {{--</div>--}}
                    {{--<div class="tags">--}}
                        {{--<a href="#">Food</a>--}}
                        {{--<a href="#">Bars</a>--}}
                        {{--<a href="#">Cooktails</a>--}}
                        {{--<a href="#">Shops</a>--}}
                        {{--<a href="#">Best Offers</a>--}}
                        {{--<a href="#">Transports</a>--}}
                        {{--<a href="#">Restaurants</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- /widget -->
            </aside>
            <!-- /aside -->
        </div>
        <!-- /row -->
    </div>
@endsection
