@extends('index')
@section('styles')
    <link href="{{asset('css/blog.css')}}" rel="stylesheet">
@endsection
@section('classBody')
    class="bg_gray"
@endsection
@section('content')
    <div class="container margin_30">
        <!-- /page_header -->
        <div class="row">
            <div class="col-lg-9">
                <div class="singlepost">
                    <figure><img alt="" class="img-fluid" src="{{asset($blog->portada)}}"></figure>
                    <h1>{{$blog->titulo}}</h1>
                    <div class="postmeta">
                        <ul>
                            <li><a href="#"><i class="ti-folder"></i> {{$blog->categoria}}</a></li>
                            <li><i class="ti-calendar"></i> {{$blog->created_at}}</li>
                            <li><a href="#"><i class="ti-user"></i> {{$blog->autor}}</a></li>
                            <li><a href="#"><i class="ti-comment"></i> {{$numeroComentarios}} Comentarios</a></li>
                        </ul>
                    </div>
                    <!-- /post meta -->
                    <div class="post-content">
                        <div class="dropcaps">
                            <p>{{$blog->cuerpo}}</p>
                        </div>
                    </div>
                    <!-- /post -->
                </div>
                <!-- /single-post -->

                <div id="comments">
                    <h5>Comentarios</h5>
                    <ul>
                        @foreach($coments as $coment)
                            <li>
                                <div class="comment_right clearfix">
                                    <div class="comment_info">
                                        Por <a href="#">{{$coment->nombre}}</a><span>|</span>{{$coment->created_at}}
                                    </div>
                                    <p>
                                        {{$coment->comentario}}
                                    </p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <hr>

                <h5>Dejanos tu comentario</h5>
                <form action="{{route('blog.comentSave')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="text" name="nombre" id="name2" class="form-control" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="text" name="email" id="email2" class="form-control" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                    <textarea class="form-control" name="comentario" id="comments2" rows="6"
                              placeholder="Comentario"></textarea>
                    </div>
                    <input type="hidden" name="idBlog" value="{{$blog->id}}">
                    <div class="form-group">
                        <button type="submit" id="submit2" class="btn_1 add_bottom_15">Enviar</button>
                    </div>
                </form>
            </div>
            <!-- /col -->

            <aside class="col-lg-3">
                <div class="widget search_blog">
                    <div class="form-group">
                        <input type="text" name="search" id="search" class="form-control" placeholder="Search..">
                        <button type="submit"><i class="ti-search"></i><span class="sr-only">Search</span></button>
                    </div>
                </div>
                <!-- /widget -->
                <div class="widget">
                    <div class="widget-title">
                        <h4>Post mas recientes</h4>
                    </div>
                    <ul class="comments-list">
                        @foreach($postRecientes as $postReciente)
                            <li>
                                <div class="alignleft">
                                    <a href="{{route('blog.find',$postReciente->slug)}}"><img src="{{asset($postReciente->portada)}}" alt="" class="img-fluid"></a>
                                </div>
                                <small>{{$postReciente->categoria}} - {{$postReciente->created_at}}</small>
                                <h3><a href="{{route('blog.find',$postReciente->slug)}}" title="">{{$postReciente->titulo}}</a></h3>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /widget -->
                <div class="widget">
                    <div class="widget-title">
                        <h4>Categorias</h4>
                    </div>
                    <ul class="cats">
                        @foreach($categorias as $categoria)
                            <li><a href="#">{{$categoria->categoria}} <span>(12)</span></a></li>
                        @endforeach
                    </ul>
                </div>
                <!-- /widget -->
                {{--<div class="widget">--}}
                    {{--<div class="widget-title">--}}
                        {{--<h4>Popular Tags</h4>--}}
                    {{--</div>--}}
                    {{--<div class="tags">--}}

                        {{--<a href="#">Food</a>--}}
                        {{--<a href="#">Bars</a>--}}
                        {{--<a href="#">Cooktails</a>--}}
                        {{--<a href="#">Shops</a>--}}
                        {{--<a href="#">Best Offers</a>--}}
                        {{--<a href="#">Transports</a>--}}
                        {{--<a href="#">Restaurants</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- /widget -->
            </aside>
            <!-- /aside -->
        </div>
        <!-- /row -->
    </div>
@endsection
