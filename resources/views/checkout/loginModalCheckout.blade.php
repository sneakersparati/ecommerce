<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
    <div class="modal_header">
        <h3>Iniciar Sesion</h3>
    </div>
    <form method="POST">
        @csrf
        <div class="sign-in-wrapper">
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email" id="email">
                <i class="ti-email"></i>
            </div>
            <div class="form-group">
                <label>Contraseña</label>
                <input type="password" class="form-control" name="password" id="password" value="">
                <i class="ti-lock"></i>
            </div>
            <div class="clearfix add_bottom_15">
                <div class="checkboxes float-left">
                    <label class="container_check">Recordarme
                        <input type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="float-right mt-1"><a id="forgot" href="javascript:void(0);">Olvidaste tu contraseña?</a>
                </div>
            </div>
            <div class="text-center">
                <input type="submit" value="Iniciar Sesion" class="btn_1 full-width">
                No tienes una cuenta? <a href="{{route('register')}}">Registrate</a>
            </div>
            <div id="forgot_pw">
                <div class="form-group">
                    <label>Por favor confirma tu email</label>
                    <input type="email" class="form-control" name="email_forgot" id="email_forgot">
                    <i class="ti-email"></i>
                </div>
                <p>Recibiras un email, con instrucciones para poder reestablecer tu contraseña.</p>
                <div class="text-center">
                    <input type="submit" value="Reestablecer contraseña" class="btn_1">
                </div>
            </div>
        </div>
    </form>
    <!--form -->
</div>
