@extends('index')
@section('styles')
    <link href="{{asset('css/checkout.css')}}" rel="stylesheet">
    <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
@endsection
@section('classBody')
    class="bg_gray"
@endsection
@section('content')
    <div class="container margin_30">
        <div class="page_header">
            <h1>Datos de la orden</h1>

        </div>
        <form action="{{route('payments.setSecret')}}" method="post" id="pay" name="pay">
        @csrf
        <!-- /page_header -->
            <div class="row">
                <div class="col-lg-4 col-md-6">

                    <div class="step first">
                        <h3>1. Dirección e información del cliente</h3>
                        @if(Auth::guest())
                            <ul class="nav nav-tabs" id="tab_checkout" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tab_1" role="tab"
                                       aria-controls="tab_3" aria-selected="false">Invitado</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tab_2" role="tab"
                                       aria-controls="tab_2" aria-selected="false">Iniciar sesion</a>
                                </li>
                            </ul>
                            <div class="tab-content checkout">
                                <div class="tab-pane fade show active" id="tab_1" role="tabpanel"
                                     aria-labelledby="tab_1">
                                    <div class="row no-gutters">
                                        <div class="col-6 form-group pr-1">
                                            <input type="text" class="form-control" placeholder="Nombre*"
                                                   name="nombreFactura" id="nombreFactura"
                                                   value="{{ old('nombreFactura') }}">
                                            @error('nombreFactura')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-6 form-group pl-1">
                                            <input type="text" class="form-control" placeholder="Apellidos*"
                                                   name="apellidosFactura" id="apellidosFactura">
                                        </div>
                                    </div>
                                    <!-- /row -->
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Correo*"
                                               name="emailFactura"
                                               id="emailFactura">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Telefono*"
                                               name="telefonoFactura" id="telefonoFactura">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Calle*" name="calleFactura"
                                               id="calleFactura">
                                    </div>
                                    <div class="row no-gutters">
                                        <div class="col-6 form-group pl-1">
                                            <input type="text" class="form-control" placeholder="Numero exterior*"
                                                   name="numExtFactura" id="numExtFactura">
                                        </div>
                                        <div class="col-6 form-group pl-1">
                                            <input type="text" class="form-control" placeholder="Numero interior"
                                                   name="numIntFactura" id="numIntFactura">
                                        </div>
                                    </div>
                                    <div class="row no-gutters">
                                        <div class="col-6 form-group pl-1">
                                            <input type="text" class="form-control" placeholder="Codigo Postal*"
                                                   name="cpFactura" id="cpFactura">
                                        </div>
                                        <div class="col-6 form-group pl-1">
                                            <select name="idColoniaFactura" class="form-control"
                                                    id="idColoniaFactura">
                                                <option value="" selected disabled hidden>Colonia*</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="tab-pane fade" id="tab_2" role="tabpanel" aria-labelledby="tab_2">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="emailLogin" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Contraseña"
                                               id="passwordLogin">
                                    </div>
                                    <div class="clearfix add_bottom_15">
                                        <div class="checkboxes float-left">
                                            <label class="container_check">Recuerdame
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="float-right"><a id="forgot" href="#0">Olvidaste tu contraseña?</a>
                                        </div>
                                    </div>
                                    <div id="forgot_pw">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email_forgot"
                                                   id="email_forgot"
                                                   placeholder="Type your email">
                                        </div>
                                        <p>A new password will be sent shortly.</p>
                                        <div class="text-center"><input type="submit" value="Reset Password"
                                                                        class="btn_1">
                                        </div>
                                    </div>
                                    <hr>
                                    {{--<input type="submit" class="btn_1 full-width" id="login" value="Iniciar sesion">--}}
                                    <button id="login" class="btn_1 full-width" type="button">Iniciar sesion</button>
                                </div>
                                <!-- /tab_2 -->
                            </div>
                        @else
                            @foreach($direcciones as $direccion)
                                <div class="box_general summary">
                                    <p>{{$direccion->alias}}</p>
                                    <p>{{$direccion->d_estado}}</p>
                                    <p>{{$direccion->d_ciudad}} {{$direccion->d_mnpio}}</p>
                                    <p>{{$direccion->d_asenta}}</p>
                                    <p>{{$direccion->calle}} {{$direccion->numeroExt}} {{$direccion->numeroInt}}</p>
                                    <p><label class="container_radio">
                                            Seleccionar dirección
                                            <input type="radio" name="idDireccion" value="{{$direccion->id}}">
                                            <span class="checkmark"></span>
                                        </label>
                                    </p>
                                </div>
                            @endforeach
                        @endif

                    </div>
                    <!-- /step -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="step first">
                        <h3>2. Metodo de pago y envio</h3>
                        <ul id="tab_checkout" role="tablist">
                            <li>
                                <label class="container_radio">
                                    Pago con tarjeta de credito o debito
                                    <input type="radio" name="payment" value="1" id="payment-tarjeta">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container_radio">
                                    Pago en efectivo
                                    <input type="radio" name="payment" value="2" id="payment-efectivo">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                        <div class="checkout">
                            <div class="tab-pane fade" id="pago-tarjeta" role="tabpanel" aria-labelledby="pago-tarjeta">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nombre del titular*"
                                           id="cardholderName" data-checkout="cardholderName">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Número de la tarjeta*"
                                           id="cardNumber"
                                           data-checkout="cardNumber" onselectstart="return false"
                                           onpaste="return false" onCopy="return false" onCut="return false"
                                           onDrag="return false" onDrop="return false" autocomplete=off>
                                </div>
                                <div class="row no-gutters">
                                    <div class="col-6 form-group pl-1">
                                        <input type="text" class="form-control" placeholder="Mes de vencimiento*"
                                               id="cardExpirationMonth" data-checkout="cardExpirationMonth"
                                               onselectstart="return false" onpaste="return false" onCopy="return false"
                                               onCut="return false" onDrag="return false" onDrop="return false"
                                               autocomplete=off>
                                    </div>
                                    <div class="col-6 form-group pl-1">
                                        <input type="text" class="form-control" placeholder="Año de vencimiento*"
                                               id="cardExpirationYear" data-checkout="cardExpirationYear"
                                               onselectstart="return false" onpaste="return false" onCopy="return false"
                                               onCut="return false" onDrag="return false" onDrop="return false"
                                               autocomplete=off>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Código de seguridad"
                                           id="securityCode" data-checkout="securityCode"
                                           onselectstart="return false" onpaste="return false" onCopy="return false"
                                           onCut="return false" onDrag="return false" onDrop="return false"
                                           autocomplete=off>
                                </div>
                                <select name="installments" class="form-control"
                                        id="installments">
                                    <option value="" selected disabled hidden>Cuotas*</option>
                                </select>
                                <input type="hidden" name="payment_method_id" id="payment_method_id"/>
                                <input type="hidden" name="transaction_amount" id="transaction_amount"
                                       value="{{$total}}"/>
                            </div>
                            <div class="tab-pane fade" id="pago-efectivo" role="tabpanel"
                                 aria-labelledby="pago-efectivo">
                                Se enviara la información con los datos de pago a tu correo electronico
                            </div>
                        </div>
                    </div>
                    <!-- /step -->

                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="step last">
                        <h3>3. Resumen de la orden</h3>
                        <div class="box_general summary">
                            <ul>
                                @foreach($cartItems as $cartItem)
                                    <li class="clearfix">
                                        <figure><img
                                                    src="{{asset($cartItem->associatedModel->url)}}"
                                                    data-src="{{asset($cartItem->associatedModel->url)}}"
                                                    alt=""
                                                    width="50"
                                                    height="50" class="lazy"></figure>
                                        <em>{{$cartItem->quantity}}x {{$cartItem->name}}</em>
                                        <span>{{'$'.number_format($cartItem->getPriceSum(), 2)}}</span></li>
                                @endforeach
                            </ul>
                            <ul>
                                <li class="clearfix"><em><strong>Subtotal</strong></em>
                                    <span>{{'$'. number_format($subTotal, 2)}}</span></li>
                                <li class="clearfix"><em><strong>Envio</strong></em> <span>$0</span></li>

                            </ul>
                            <div class="total clearfix">TOTAL <span>{{'$'. number_format($total, 2)}}</span></div>
                            <div class="form-group">
                                <label class="container_check">Acepto terminos y condiciones y politicas de envio.
                                    <input type="checkbox" checked>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container_check">Registrarme al Newsletter.
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            @if($total == 0)
                                <a href="{{route('home')}}" class="btn_1 full-width">Añadir productos al carrito</a>
                            @else
                                <button type="submit" class="btn_1 full-width">Confirmar y pagar</button>
                            @endif
                        </div>
                        <!-- /box_general -->
                    </div>
                    <!-- /step -->
                </div>
            </div>
            <!-- /row -->
        </form>
    </div>
@endsection

@section('scripts')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        // Other address Panel
        $('#other_addr input').on("change", function () {
            if (this.checked)
                $('#other_addr_c').fadeIn('fast');
            else
                $('#other_addr_c').fadeOut('fast');
        });
    </script>

    <script>
        window.Mercadopago.setPublishableKey("APP_USR-81e32963-12b0-43a4-9b64-48816dcf3664");

        document.getElementById('cardNumber').addEventListener('keyup', guessPaymentMethod);

        function guessPaymentMethod(event) {
            let cardnumber = document.getElementById("cardNumber").value;

            if (cardnumber.length >= 6) {
                let bin = cardnumber.substring(0, 6);
                window.Mercadopago.getPaymentMethod({
                    "bin": bin
                }, setPaymentMethod);
            }
        };

        function setPaymentMethod(status, response) {
            if (status == 200) {
                let paymentMethodId = response[0].id;
                let element = document.getElementById('payment_method_id');
                element.value = paymentMethodId;
                getInstallments();
            } else {
                alert(`payment method info error: ${response}`);
            }
        }

        function getInstallments() {
            window.Mercadopago.getInstallments({
                "payment_method_id": document.getElementById('payment_method_id').value,
                "amount": parseFloat(document.getElementById('transaction_amount').value)

            }, function (status, response) {
                if (status == 200) {
                    document.getElementById('installments').options.length = 0;
                    response[0].payer_costs.forEach(installment => {
                        let opt = document.createElement('option');
                        opt.text = installment.recommended_message;
                        opt.value = installment.installments;
                        document.getElementById('installments').appendChild(opt);
                    });
                } else {
                    alert(`installments method info error: ${response}`);
                }
            });
        }

        doSubmit = false;
        $('#payment-tarjeta').on('click', function (e) {
            $('#pago-tarjeta').removeClass('hide');
            $('#pago-tarjeta').addClass('show');
            $('#pago-efectivo').removeClass('show');
            $('#pago-efectivo').addClass('hide');
            document.querySelector('#pay').addEventListener('submit', doPay);
        });
        $('#payment-efectivo').on('click', function (e) {
            $('#pago-efectivo').removeClass('hide');
            $('#pago-efectivo').addClass('show');
            $('#pago-tarjeta').removeClass('show');
            $('#pago-tarjeta').addClass('hide');
        });


        function doPay(event) {

            event.preventDefault();
            if (!doSubmit) {
                var $form = document.querySelector('#pay');
                window.Mercadopago.createToken($form, sdkResponseHandler);
                return false;
            }
        }

        function sdkResponseHandler(status, response) {
            if (status != 200 && status != 201) {
                alert("verify filled data");
            } else {
                var form = document.querySelector('#pay');
                var card = document.createElement('input');
                card.setAttribute('name', 'token');
                card.setAttribute('type', 'hidden');
                card.setAttribute('value', response.id);
                form.appendChild(card);
                doSubmit = true;
                form.submit();
            }
        };
    </script>
    <script>
        $('#cpFactura').on('keyup', function (e) {
            var cp = e.target.value;
            if (cp.length === 5) {
                $.get('colonias/' + cp, function (data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#idColoniaFactura').append('<option value="' + data[i].id + '">' + data[i].d_asenta + '</option>');
                    }
                })
            }
        })
    </script>
    <script>
        $('#login').on('click', function () {
            var email = $('#emailLogin').val();
            var password = $('#passwordLogin').val();
            $.post('{{route('user.login')}}',
                {
                    "_token": "{{ csrf_token() }}",
                    "email": email,
                    "password": password
                },
                function (data) {
                    swal({
                        title: "Good job!",
                        text: "You clicked the button!",
                        icon: "success",
                    });
                }
            );
        })
    </script>
@endsection
