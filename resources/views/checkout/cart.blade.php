@extends('index')
@section('styles')
    <link href="{{asset('css/cart.css')}}" rel="stylesheet">
@endsection
@section('classBody')
    class="bg_gray"
@endsection
@section('content')
    <div class="container margin_30">
        <div class="page_header">
            <h1>Carrito de compras</h1>
        </div>
        <!-- /page_header -->
        <table class="table table-striped cart-list">
            <thead>
            <tr>
                <th>
                    Producto
                </th>
                <th>
                    Precio
                </th>
                <th>
                    Cantidad
                </th>
                <th>
                    Talla
                </th>
                <th>
                    Subtotal
                </th>
                <th>

                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($cartItems as $cartItem)
                <tr>
                    <td>
                        <div class="thumb_cart">
                            <img src="{{asset($cartItem->associatedModel->url)}}"
                                 data-src="{{asset($cartItem->associatedModel->url)}}"
                                 class="lazy" alt="Image">
                        </div>
                        <span class="item_cart">{{$cartItem->name}}</span>
                    </td>
                    <td>
                        <strong>{{$cartItem->associatedModel->formatoPrecio()}}</strong>
                    </td>
                    <td>
                        <div class="numbers-row">
                            <input type="text" value="{{$cartItem->quantity}}" id="quantity" class="qty2"
                                   name="quantity" disabled>
                            <div class="inc button_inc">+</div>
                            <div class="dec button_inc">-</div>
                        </div>
                    </td>
                    <td>
                        {{$cartItem->attributes->talla}}
                    </td>
                    <td>
                        <strong>{{'$'.number_format($cartItem->getPriceSum(), 2)}}</strong>
                    </td>
                    <td class="options">
                        <form action="{{route('cart.destroy')}}" method="POST">
                            @csrf
                            <input name="_method" type="hidden" value="PUT">
                            <input type="hidden" value="{{$cartItem->id}}" name="id" id="id">
                            <button type="submit" style="border: 0; background-color: transparent;"><i
                                        class="ti-trash"></i></button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="row add_top_30 flex-sm-row-reverse cart_actions">
            {{--<div class="col-sm-4 text-right">--}}
            {{--<button type="button" class="btn_1 gray">Actualizar carrito</button>--}}
            {{--</div>--}}
            <div class="col-sm-8">
                <div class="apply-coupon">
                    <div class="form-group form-inline">
                        <input type="text" name="coupon-code" value="" placeholder="Codigo de promocion"
                               class="form-control">
                        <button type="button" class="btn_1 outline">Aplicar cupon</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /cart_actions -->

    </div>
    <div class="box_cart">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <ul>
                        <li>
                            <span>Subtotal</span> {{'$'. number_format($subTotal, 2)}}
                        </li>
                        <li>
                            <span>Envio</span> $7.00
                        </li>
                        <li>
                            <span>Total</span> {{'$'. number_format($total, 2)}}
                        </li>
                    </ul>
                    @if(Auth::guest())
                        <a href="#sign-in-dialog-2" id="sign-in-3" class="btn_1">Tramitar el pedido</a>
                    @endif
                    @if(!Auth::guest())
                        <a href="{{route('checkout.index')}}" class="btn_1">Tramitar el pedido</a>
                    @endif
                </div>
            </div>
        </div>
    </div>


    <div id="sign-in-dialog-2" class="zoom-anim-dialog mfp-hide">
        <h4 class="text-center pt-4">¿No tienes cuenta?</h4>
        <div class="text-center">
            <a class="btn_1 mt-3" href="{{route('checkout.index')}}">Continuar como invitado</a>
        </div>
        <h4 class="text-center mt-3">¿Ya tienes una cuenta?</h4>
        <form method="POST" class="mt-3" action="{{route('cart.login')}}">
            @csrf
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email" id="email">
                <i class="ti-email"></i>
            </div>
            <div class="form-group">
                <label>Contraseña</label>
                <input type="password" class="form-control" name="password" id="password" value="">
                <i class="ti-lock"></i>
            </div>
            <div class="clearfix add_bottom_15">
                <div class="checkboxes float-left">
                    <label class="container_check">Recordarme
                        <input type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="float-right mt-1"><a id="forgot" href="javascript:void(0);">Olvidaste tu contraseña?</a>
                </div>
            </div>
            <div class="text-center">
                <input type="submit" value="Iniciar Sesion" class="btn_1 full-width">
            </div>
        </form>
        <div class="text-center">
            No tienes una cuenta? <a href="{{route('register')}}">Registrate</a>
        </div>
        <div id="forgot_pw">
            <div class="form-group">
                <label>Por favor confirma tu email</label>
                <input type="email" class="form-control" name="email_forgot" id="email_forgot">
                <i class="ti-email"></i>
            </div>
            <p>Recibiras un email, con instrucciones para poder reestablecer tu contraseña.</p>
            <div class="text-center">
                <input type="submit" value="Reestablecer contraseña" class="btn_1">
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('js/app.js')}}"></script>
    <script>
        (function () {
            const inc = document.querySelectorAll('.inc');
            const dec = document.querySelectorAll('.dec');
            Array.from(inc).forEach(function (element) {
                element.addEventListener('click', function () {
                    const id = document.getElementById('id').value;
                    const quantity = document.getElementById('quantity').value;
                    axios.patch(`/cart/update/${id}`, {
                        quantity: quantity
                    })
                        .then(function (response) {
                            window.location.reload();
                        })
                        .catch(function (error) {
                            window.location.reload();
                        })
                })
            });
            Array.from(dec).forEach(function (element) {
                element.addEventListener('click', function () {
                    const id = document.getElementById('id').value;
                    const quantity = document.getElementById('quantity').value;
                    axios.patch(`/cart/update/${id}`, {
                        quantity: quantity
                    })
                        .then(function (response) {
                            window.location.reload();
                        })
                        .catch(function (error) {
                            window.location.reload();
                        })
                })
            });
        })();
    </script>
@endsection
