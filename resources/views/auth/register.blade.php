@extends('index')
@section('styles')
    <link href="{{asset('css/account.css')}}" rel="stylesheet">
@endsection
@section('classBody')
    class="bg_gray"
@endsection
@section('content')
    <div class="container margin_30">
        <div class="page_header">
            {{--<div class="breadcrumbs">--}}
            {{--<ul>--}}
            {{--<li><a href="#">Home</a></li>--}}
            {{--<li><a href="#">Category</a></li>--}}
            {{--<li>Page active</li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            <h1>Inicia sesion o registrate</h1>
        </div>
        <!-- /page_header -->
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-8">
                <div class="box_account">
                    <h3 class="client">Ya tengo una cuenta</h3>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form_container">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email*">
                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" id="password_in" value=""
                                       placeholder="Contraseña*">
                                @if ($errors->has('password'))
                                    <span class="text-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="clearfix add_bottom_15">
                                <div class="checkboxes float-left">
                                    <label class="container_check">Recuerdame
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="float-right"><a id="forgot" href="javascript:void(0);">Olvidaste tu
                                        contraseña?</a></div>
                            </div>
                            <div class="text-center"><input type="submit" value="Iniciar Sesion"
                                                            class="btn_1 full-width">
                            </div>
                            <div id="forgot_pw">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email_forgot" id="email_forgot"
                                           placeholder="Type your email">
                                </div>
                                <p>A new password will be sent shortly.</p>
                                <div class="text-center"><input type="submit" value="Reset Password" class="btn_1">
                                </div>
                            </div>
                        </div>
                    </form>

                    <!-- /form_container -->
                </div>
                <!-- /box_account -->
                <div class="row">
                    <div class="col-md-6 d-none d-lg-block">
                        <ul class="list_ok">
                            <li>Promociones exclusivas</li>
                            <li>Soporte via telefonica</li>
                            <li>Protección de datos</li>
                        </ul>
                    </div>
                    <div class="col-md-6 d-none d-lg-block">
                        <ul class="list_ok">
                            <li>PAgo 100% seguro</li>
                            <li>Soporte 24H</li>
                        </ul>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <div class="col-xl-6 col-lg-6 col-md-8">
                <div class="box_account">
                    <h3 class="new_client">Quiero registrarme</h3>
                    <small class="float-right pt-2">* Campos requeridos</small>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form_container">
                            <div class="form-group">
                                <input type="email" class="form-control" name="emailRegister" id="email"
                                       placeholder="Email*"
                                       value="{{ old('emailRegister') }}">
                                @error('emailRegister')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="passwordRegister" id="password"
                                       value=""
                                       placeholder="Contraseña*">
                                @error('passwordRegister')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" name="password-confirm"
                                       id="password_confirmation" value=""
                                       placeholder="Confirmar contraseña *">
                                @error('password-confirm')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <hr>
                            <div class="private box">
                                <div class="row no-gutters">
                                    <div class="col-6 pr-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Nombre*" name="name"
                                                   value="{{ old('name') }}">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-6 pl-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Apellidos*"
                                                   name="apellidos" value="{{ old('apellidos') }}">
                                            @error('apellidos')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                                <!-- /row -->
                                <!-- /row -->

                                <div class="row no-gutters">
                                    {{--<div class="col-6 pr-1">--}}
                                    {{--<div class="form-group">--}}
                                    {{--<div class="custom-select-form">--}}
                                    {{--<select class="wide add_bottom_10" name="country" id="country">--}}
                                    {{--<option value="" selected>Country*</option>--}}
                                    {{--<option value="Europe">Europe</option>--}}
                                    {{--<option value="United states">United states</option>--}}
                                    {{--<option value="Asia">Asia</option>--}}
                                    {{--</select>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="col-6 pl-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Telefono *"
                                                   name="telefono" value="{{ old('telefono') }}">
                                            @error('telefono')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-6 pl-1">
                                        <div class="form-group">
                                            <input type="date" class="form-control" placeholder="Fecha de nacimiento"
                                                   name="fechaNacimiento" value="{{ old('fechaNacimiento') }}">
                                        </div>
                                    </div>
                                </div>
                                <!-- /row -->

                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="container_check">Acepto <a href="#0">Terminos y condiciones</a>
                                    <input type="checkbox" name="terminosCondiciones">
                                    <span class="checkmark"></span>
                                </label>
                                @error('terminosCondiciones')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="container_check">Suscribirme a Newsletter
                                    <input type="checkbox" name="newsletter">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="text-center"><input type="submit" value="Registrar" class="btn_1 full-width">
                            </div>
                        </div>
                    </form>

                    <!-- /form_container -->
                </div>
                <!-- /box_account -->
            </div>
        </div>
        <!-- /row -->
    </div>
@endsection
@section('scripts')
    <script>
        // Client type Panel
        $('input[name="client_type"]').on("click", function () {
            var inputValue = $(this).attr("value");
            var targetBox = $("." + inputValue);
            $(".box").not(targetBox).hide();
            $(targetBox).show();
        });
    </script>
@endsection
