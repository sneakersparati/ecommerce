<header class="version_1">
    <div class="layer"></div><!-- Mobile menu overlay mask -->
    <div class="main_header">
        <div class="container">
            <div class="row small-gutters">
                <div class="col-xl-3 col-lg-3 d-lg-flex align-items-center">
                    <div id="logo">
                        <a href="{{route('home')}}"><img src="{{asset('img/logo_pagina.png')}}" alt="" width="80" height="85"></a>
                    </div>
                </div>
                <nav class="col-xl-6 col-lg-7">
                    <a class="open_close" href="javascript:void(0);">
                        <div class="hamburger hamburger--spin">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                    </a>
                    <!-- Mobile menu button -->
                    <div class="main-menu">
                        <div id="header_menu">
                            <a href="{{route('home')}}"><img src="{{asset('img/logo_pagina.png')}}" alt="" width="100" height="50"></a>
                            <a href="#" class="open_close" id="close_in"><i class="ti-close"></i></a>
                        </div>
                        <ul>
                            {{--<li class="submenu">--}}
                            {{--<a href="javascript:void(0);" class="show-submenu">Home</a>--}}
                            {{--<ul>--}}
                            {{--<li><a href="index.html">Slider</a></li>--}}
                            {{--<li><a href="index-2.html">Video Background</a></li>--}}
                            {{--<li><a href="index-3.html">Vertical Slider</a></li>--}}
                            {{--<li><a href="index-4.html">GDPR Cookie Bar</a></li>--}}
                            {{--</ul>--}}
                            {{--</li>--}}
                            <li>
                                <a href="{{route('home')}}">Inicio</a>
                            </li>
                            <li class="megamenu submenu">
                                <a href="javascript:void(0);" class="show-submenu-mega">Sneakers</a>
                                <div class="menu-wrapper">
                                    <div class="row small-gutters">
                                        @foreach($subCategorias as $subCategoria)
                                            <div class="col-lg-3">
                                                <h3>{{$subCategoria->categoria}}</h3>
                                                <ul>
                                                    @foreach($marcas as $marca)
                                                        <li>
                                                            <a href="{{route('product.collection',[$subCategoria->id,$marca->id])}}">{{$marca->descripcion}}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endforeach

                                    </div>
                                    <!-- /row -->
                                </div>
                                <!-- /menu-wrapper -->
                            </li>
                            <li>
                                <a href="{{route('blog.index')}}">Blog</a>
                            </li>
                        </ul>
                    </div>
                    <!--/main-menu -->
                </nav>
                <div class="col-xl-3 col-lg-2 d-lg-flex align-items-center justify-content-end text-right">
                    <a class="phone_top"
                       href="tel://5530575652"><strong><span>Necesitas ayuda?</span>55-30-57-56-52</strong></a>
                </div>
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /main_header -->

    <div class="main_nav Sticky">
        <div class="container">
            <div class="row small-gutters">
                <div class="col-xl-3 col-lg-3 col-md-3">
                    <nav class="categories">
                        <ul class="clearfix">
                            <li><span>
										<a href="#">
											<span class="hamburger hamburger--spin">
												<span class="hamburger-box">
													<span class="hamburger-inner"></span>
												</span>
											</span>
											Marcas
										</a>
									</span>
                                <div id="menu">
                                    <ul>
                                        @foreach($marcas as $marca)
                                            <li>
                                                <a href="{{route('product.collection.idMarca',$marca->id)}}">{{$marca->descripcion}}</a>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xl-6 col-lg-7 col-md-6 d-none d-md-block">
                    <!-- <div class="custom-search-input">
                        <input type="text" placeholder="Busca el producto que necesites" id="searchInput">
                        <button type="submit"><i class="header-icon_search_custom"></i></button>
                    </div> -->
                </div>
                <div class="col-xl-3 col-lg-2 col-md-3">
                    <ul class="top_tools">
                        <li>
                            <div class="dropdown dropdown-cart">
                                <a href="{{route('cart.index')}}"
                                   class="cart_bt"><strong>{{$cartTotalQuantity}}</strong></a>
                                <div class="dropdown-menu">
                                    <ul>
                                        @foreach($cartItems as $cartItem)
                                            <li>
                                                <a href="{{route('product.detail',$cartItem->associatedModel->link)}}">
                                                    <figure><img
                                                                src="{{asset($cartItem->associatedModel->url)}}"
                                                                data-src="{{asset($cartItem->associatedModel->url)}}"
                                                                alt=""
                                                                width="50"
                                                                height="50" class="lazy"></figure>
                                                    <strong><span>{{$cartItem->quantity}}
                                                            x {{$cartItem->name}}</span>{{$cartItem->associatedModel->formatoPrecio()}}
                                                    </strong>
                                                </a>
                                                <form action="{{route('cart.destroy')}}" method="POST"
                                                      name="formDelete" id="formDelete">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="PUT">
                                                    <input type="hidden" value="{{$cartItem->id}}" name="id" id="id">
                                                    <a href="javascript:;"
                                                       onclick="document.getElementById('formDelete').submit()"
                                                       class="action"><i class="ti-trash"></i></a>
                                                </form>

                                            </li>
                                        @endforeach

                                    </ul>
                                    <div class="total_drop">
                                        <div class="clearfix">
                                            <strong>Total</strong><span>{{'$'. number_format($total, 2)}}</span></div>
                                        <a href="{{route('cart.index')}}" class="btn_1 outline">Ver carrito</a>
                                        @if(!Auth::guest())
                                            <a href="{{route('checkout.index')}}" class="btn_1">Finalizar
                                                pedido</a>
                                        @else
                                            <a href="#sign-in-dialog-2" id="checkout-2" class="btn_1">Tramitar el
                                                pedido</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- /dropdown-cart-->
                        </li>
                        <!-- <li>
                            <a href="#search-dialog" class="wishlist"><span>Wishlist</span></a>
                        </li> -->
                        <li>
                            <div class="dropdown dropdown-access">
                                <a href="account.html" class="access_link"><span>Account</span></a>
                                <div class="dropdown-menu">
                                    @if(!Auth::guest())
                                        <a id="sign-in" class="btn_1" href="{{ route('user.logout') }}"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Cerrar sesion
                                        </a>
                                        <form id="logout-form" action="{{ route('user.logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    @else
                                        <a href="#sign-in-dialog" id="sign-in" class="btn_1">Inicia sesion o
                                            registrate</a>
                                    @endif
                                    <ul>
                                        @if(!Auth::guest())
                                            <li>
                                                <a href="{{route('direcciones.index')}}"><i
                                                            class="ti-location-arrow"></i>Mis direcciones</a>
                                            </li>
                                            <li>
                                                <a href="{{route('user.perfil')}}"><i class="ti-user"></i>Mi cuenta</a>
                                            </li>
                                            <li>
                                                <a href="{{route('orders.misOrdenes')}}"><i class="ti-package"></i>Mis
                                                    ordenes</a>
                                            </li>
                                        @endif
                                        {{--<li>--}}
                                        {{--<a href="{{route('trackOrder.index')}}"><i class="ti-truck"></i>Rastrea tu--}}
                                        {{--pedido</a>--}}
                                        {{--</li>--}}
                                        <li>
                                            <a href="{{route('faq.index')}}"><i class="ti-help-alt"></i>Ayuda y
                                                preguntas frecuentes</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /dropdown-access-->
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="btn_search_mob"><span>Buscar</span></a>
                        </li>
                        <li>
                            <a href="#menu" class="btn_cat_mob">
                                <div class="hamburger hamburger--spin" id="hamburger">
                                    <div class="hamburger-box">
                                        <div class="hamburger-inner"></div>
                                    </div>
                                </div>
                                Marcas
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /row -->
        </div>
        <div class="search_mob_wp">
            <div class="row">
                <div class="col-12">
                    <input type="text" class="form-control full-width" placeholder="Busca el producto que necesites"
                           id="searchInput2">
                    <input type="submit" class="btn_1 full-width" value="Buscar">
                </div>
            </div>

        </div>
        <!-- /search_mobile -->
    </div>
    <!-- /main_nav -->
</header>

<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide" role="dialog">
    <div class="modal_header">
        <h3>Iniciar Sesion</h3>
    </div>
    <form method="POST" action="{{route('user.login')}}">
        @csrf
        <div class="sign-in-wrapper">
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email" id="email">
                <i class="ti-email"></i>
            </div>
            <div class="form-group">
                <label>Contraseña</label>
                <input type="password" class="form-control" name="password" id="password" value="">
                <i class="ti-lock"></i>
            </div>
            <div class="clearfix add_bottom_15">
                <div class="checkboxes float-left">
                    <label class="container_check">Recordarme
                        <input type="checkbox">
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="float-right mt-1"><a id="forgot" href="javascript:void(0);">Olvidaste tu contraseña?</a>
                </div>
            </div>
            <div class="text-center">
                <input type="submit" value="Iniciar Sesion" class="btn_1 full-width">
                No tienes una cuenta? <a href="{{route('register')}}">Registrate</a>
            </div>
            <div id="forgot_pw">
                <div class="form-group">
                    <label>Por favor confirma tu email</label>
                    <input type="email" class="form-control" name="email_forgot" id="email_forgot">
                    <i class="ti-email"></i>
                </div>
                <p>Recibiras un email, con instrucciones para poder reestablecer tu contraseña.</p>
                <div class="text-center">
                    <input type="submit" value="Reestablecer contraseña" class="btn_1">
                </div>
            </div>
        </div>
    </form>
    <!--form -->
</div>

<div id="sign-in-dialog-2" class="zoom-anim-dialog mfp-hide">
    <h4 class="text-center pt-4">¿No tienes cuenta?</h4>
    <div class="text-center">
        <a class="btn_1 mt-3" href="{{route('checkout.index')}}">Continuar como invitado</a>
    </div>
    <h4 class="text-center mt-3">¿Ya tienes una cuenta?</h4>
    <form method="POST" class="mt-3" action="{{route('cart.login')}}">
        @csrf
        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" name="email" id="email">
            <i class="ti-email"></i>
        </div>
        <div class="form-group">
            <label>Contraseña</label>
            <input type="password" class="form-control" name="password" id="password" value="">
            <i class="ti-lock"></i>
        </div>
        <div class="clearfix add_bottom_15">
            <div class="checkboxes float-left">
                <label class="container_check">Recordarme
                    <input type="checkbox">
                    <span class="checkmark"></span>
                </label>
            </div>
            <div class="float-right mt-1"><a id="forgot" href="javascript:void(0);">Olvidaste tu contraseña?</a>
            </div>
        </div>
        <div class="text-center">
            <input type="submit" value="Iniciar Sesion" class="btn_1 full-width">
        </div>
    </form>
    <div class="text-center">
        No tienes una cuenta? <a href="{{route('register')}}">Registrate</a>
    </div>
    <div id="forgot_pw">
        <div class="form-group">
            <label>Por favor confirma tu email</label>
            <input type="email" class="form-control" name="email_forgot" id="email_forgot">
            <i class="ti-email"></i>
        </div>
        <p>Recibiras un email, con instrucciones para poder reestablecer tu contraseña.</p>
        <div class="text-center">
            <input type="submit" value="Reestablecer contraseña" class="btn_1">
        </div>
    </div>
</div>
