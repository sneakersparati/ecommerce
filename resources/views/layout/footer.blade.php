<footer class="revealed">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <h3 data-target="#collapse_1">Enlaces de interes</h3>
                <div class="collapse dont-collapse-sm links" id="collapse_1">
                    <ul>
                        <li><a href="{{route('about.index')}}">Acerca de nosotros</a></li>
                        <li><a href="{{route('faq.index')}}">Preguntas frecuentes</a></li>
                        {{--<li><a href="help.html">Aviso de privacidad</a></li>--}}
                        @if(!Auth::guest())
                            <li><a href="{{route('user.perfil')}}">Mi cuenta</a></li>
                        @endif
                        <li><a href="{{route('blog.index')}}">Blog</a></li>
                        <li><a href="{{route('contact.index')}}">Contacta con nosotros</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <h3 data-target="#collapse_2">Categorias</h3>
                <div class="collapse dont-collapse-sm links" id="collapse_2">
                    <ul>
                        @foreach($categorias as $categoria)
                            <li>
                                <a href="{{route('product.collection.categoria',$categoria->id)}}">{{$categoria->categoria}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <h3 data-target="#collapse_3">Contacto</h3>
                <div class="collapse dont-collapse-sm contacts" id="collapse_3">
                    <ul>
                        {{--<li><i class="ti-home"></i>97845 Baker st. 567<br>Los Angeles - US</li>--}}
                        <li><i class="ti-headphone-alt"></i>55-30-57-56-52</li>
                        <li><i class="ti-email"></i><a href="#0">ventas@sneakersparati.com.mx</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <h3 data-target="#collapse_4">Mantente actualizado</h3>
                <div class="collapse dont-collapse-sm" id="collapse_4">
                    <div id="newsletter">
                        <form action="{{route('newsletter.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control"
                                       placeholder="Deja tu email">
                                <button type="submit" id="submit-newsletter"><i class="ti-angle-double-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="follow_us">
                        <h5>Siguenos</h5>
                        <ul>
                            {{--<li><a href="#0"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" data-src="img/twitter_icon.svg" alt="" class="lazy"></a></li>--}}
                            <li><a href="https://www.facebook.com/sneakersParaTi"><img
                                            src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                            data-src="{{asset('img/facebook_icon.svg')}}" alt="" class="lazy"></a></li>
                            <li><a href="https://www.instagram.com/sneakers_para_ti_2020"><img
                                            src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                            data-src="{{asset('img/instagram_icon.svg')}}" alt="" class="lazy"></a></li>
                            {{--<li><a href="#0"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" data-src="img/youtube_icon.svg" alt="" class="lazy"></a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /row-->
        <hr>
        <div class="row add_bottom_25">
            <div class="col-lg-6">
                <ul class="footer-selector clearfix">
                    {{--<li>--}}
                    {{--<div class="styled-select lang-selector">--}}
                    {{--<select>--}}
                    {{--<option value="English" selected>English</option>--}}
                    {{--<option value="French">French</option>--}}
                    {{--<option value="Spanish">Spanish</option>--}}
                    {{--<option value="Russian">Russian</option>--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<div class="styled-select currency-selector">--}}
                    {{--<select>--}}
                    {{--<option value="US Dollars" selected>US Dollars</option>--}}
                    {{--<option value="Euro">Euro</option>--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    {{--</li>--}}
                    <li><img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                             data-src="{{asset('img/cards_all.svg')}}" alt="" width="198" height="30" class="lazy"></li>
                </ul>
            </div>
            <div class="col-lg-6">
                <ul class="additional_links">
                    <li><a href="{{route('terms.index')}}">Terminos y condiciones</a></li>
                    <li><a href="{{route('shipingPolicy.index')}}">Politicas de envio</a></li>
                    <li><span>© 2020 Sneakers para ti</span></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
