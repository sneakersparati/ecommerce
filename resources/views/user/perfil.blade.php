@extends('index')
@section('styles')
    <link href="{{asset('css/account.css')}}" rel="stylesheet">
@endsection
@section('classBody')
    class="bg_gray"
@endsection
@section('content')
    <div class="container margin_30">
        <div class="page_header">
            <h1>Detalles de tu cuenta</h1>
        </div>
        <!-- /page_header -->
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="box_account">
                    <form action="{{route('user.perfilUpdate',$usuario->id)}}" method="POST">
                        @csrf
                        <div class="form_container">
                            <div class="private box">
                                <div class="row no-gutters">
                                    <div class="col-6 pr-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Nombre"
                                                   value="{{$usuario->name}}" name="name">
                                        </div>
                                    </div>
                                    <div class="col-6 pl-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Apellidos"
                                                   value="{{$usuario->apellidos}}" name="apellidos">
                                        </div>
                                    </div>
                                    <div class="col-12 pl-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Email"
                                                   value="{{$usuario->email}}" name="email">
                                        </div>
                                    </div>
                                    <div class="col-6 pl-1">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Telefono"
                                                   value="{{$usuario->telefono}}" name="telefono">
                                        </div>
                                    </div>
                                    <div class="col-6 pl-1">
                                        <div class="form-group">
                                            <input type="date" class="form-control"
                                                   value="{{$usuario->fechaNacimiento}}" name="fechaNacimiento">
                                        </div>
                                    </div>
                                </div>

                                <!-- /row -->

                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="container_check">Newsletter
                                    @if($usuario->newsletter == 1)
                                        <input type="checkbox" checked value="{{$usuario->newsletter}}"
                                               name="newsletter">
                                    @else
                                        <input type="checkbox" value="1" name="newsletter">
                                    @endif
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="text-center"><input type="submit" value="Actualizar informacion"
                                                            class="btn_1 full-width"></div>
                        </div>
                    </form>
                    <!-- /form_container -->
                </div>
                <!-- /box_account -->
            </div>
        </div>
        <!-- /row -->
    </div>
@endsection
