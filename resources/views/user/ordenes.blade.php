@extends('index')
@section('styles')
    <link href="{{asset('css/account.css')}}" rel="stylesheet">
@endsection
@section('classBody')
    class="bg_gray"
@endsection
@section('content')
    <div class="container margin_30">
        <div class="page_header">
            <h1>Mis ordenes</h1>
        </div>
        <div class="row justify-content-center text-center">
            <div class="col-xl-7 col-lg-9">
                <img src="{{asset('img/ventas.svg')}}" alt="" class="img-fluid add_bottom_15" width="200" height="177">
                <h3>Aun no se han realizado compras</h3>
                <a href="{{route('product.collection.categoria', 2)}}" class="btn_1 mt-3">Comenzar a comprar</a>
            </div>
        </div>
        <div class="row mt-5">
            @foreach($ordenes as $orden)
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <div class="box_account">
                        <div class="form_container">
                            <div class="row justify-content-between">
                                <div class="col-md-8">
                                    <p>Total: {{'$'. number_format($orden->total, 2)}}</p>
                                    <p>Nombre: {{$orden->nombreFactura." ".$orden->apellidosFactura}}</p>
                                    <p>Telefono: {{$orden->telefonoFactura}}</p>
                                    <p>Email: {{$orden->emailFactura}}</p>
                                </div>
                                <div class="col-md-4">
                                    <a href="#direcciones-dialog" id="view-orden-btn" class="btn_1"
                                       data-id="{{$orden->id}}"><i class="ti-eye"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- /form_container -->
                    </div>
                    <!-- /box_account -->
                </div>
            @endforeach
        </div>
        <!-- /row -->
    </div>

    <div id="direcciones-dialog" class="zoom-anim-dialog mfp-hide" role="dialog">
        <div class="modal_header">
            <h3>Detalle de tu orden</h3>
        </div>
        <div class="sign-in-wrapper">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Cantidad
                    </th>
                    <th>
                        Talla
                    </th>
                    <th>
                        Precio
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr id="productos">

                </tr>
                </tbody>
            </table>
        </div>
        <!--form -->
    </div>
@endsection
@section('scripts')
    <script>
        $('#view-orden-btn').on('click', function (e) {
            var orderId = $(this).data('id');
            $.get('detalle-orden/' + orderId, function (data) {
                for (var i = 0; i < data.length; i++) {
                    $('#productos').append('<td>' + data[i].titulo + '</td>');
                    $('#productos').append('<td>' + data[i].cantidad + '</td>');
                    $('#productos').append('<td>' + data[i].talla + '</td>');
                }
            })
        })
    </script>
@endsection
