@extends('index')
@section('styles')
    <link href="{{asset('css/account.css')}}" rel="stylesheet">
@endsection
@section('classBody')
    class="bg_gray"
@endsection
@section('content')
    <div class="container margin_30">
        <div class="page_header">
            <h1>Mis direcciones</h1>
        </div>
        <div class="row">
            <div class="container">
                <a href="#direcciones-dialog" id="direcciones" class="btn_1"><i class="ti-plus"></i> Agregar
                    Dirección</a>
            </div>
        </div>
        <div class="row mt-5">
            @foreach($direcciones as $direccion)
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <div class="box_account">
                        <div class="form_container">
                            <div class="row justify-content-between">
                                <div class="col-md-8">
                                    <p>{{$direccion->alias}}</p>
                                    <p>{{$direccion->d_estado}}</p>
                                    <p>{{$direccion->d_mnpio}}</p>
                                    <p>{{$direccion->d_asenta}}</p>
                                    <p>{{$direccion->calle}} {{$direccion->numeroExt}} {{$direccion->numeroInt}}</p>
                                </div>
                                <div class="col-md-4">
                                    <a href="#direcciones-edit" id="direcciones-edit-btn-{{$direccion->id}}"
                                       class="btn_1 direccion-btn" data-id="{{$direccion->id}}"><i
                                                class="ti-pencil"></i></a>
                                    <form action="{{route('direcciones.destroy')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$direccion->id}}">
                                        <button class="btn_1 btn-outline-warning mt-md-3" type="submit"><i
                                                    class="ti-trash"></i></button>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /form_container -->
                    </div>
                    <!-- /box_account -->
                </div>
            @endforeach
        </div>
        <!-- /row -->
    </div>

    <div id="direcciones-dialog" class="zoom-anim-dialog mfp-hide" role="dialog">
        <div class="modal_header">
            <h3>Agregar Dirección</h3>
        </div>
        <form method="POST" action="{{route('direcciones.store')}}">
            @csrf
            <div class="sign-in-wrapper">
                <div class="form-group">
                    <input type="text" class="form-control" name="alias" id="alias" placeholder="Alias">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="calle" id="calle" placeholder="Calle">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="numeroExt" id="numeroExt"
                           placeholder="Numero exterior">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="numeroInt" id="numeroInt"
                           placeholder="Numero interior">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="cp" id="cp" placeholder="Codigo postal">
                </div>
                <div class="form-group">
                    <select name="idColonia" class="form-control"
                            id="idColonia">
                        <option value="" selected disabled hidden>Colonia*</option>
                    </select>
                </div>
                <input type="hidden" name="idUsuario" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">
                <div class="text-center">
                    <input type="submit" value="Agregar dirección" class="btn_1 full-width">
                </div>
            </div>
        </form>
        <!--form -->
    </div>
    <div id="direcciones-edit" class="zoom-anim-dialog mfp-hide" role="dialog">
        <div class="modal_header">
            <h3>Editar Dirección</h3>
        </div>
        <form method="POST" action="{{route('direcciones.update')}}">
            @csrf
            <div class="sign-in-wrapper">
                <div class="form-group">
                    <input type="text" class="form-control" name="alias" id="aliasUpdate" placeholder="Alias">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="calle" id="calleUpdate" placeholder="Calle">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="numeroExt" id="numeroExtUpdate"
                           placeholder="Numero exterior">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="numeroInt" id="numeroIntUpdate"
                           placeholder="Numero interior">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="cp" id="cpUpdate" placeholder="Codigo postal">
                </div>
                <div class="form-group">
                    <select name="idColonia" class="form-control"
                            id="idColoniaUpdate">
                        <option value="" selected disabled hidden>Colonia*</option>
                    </select>
                </div>
                <input type="hidden" name="idUsuario" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">
                <input type="hidden" name="id" id="idUpdate">
                <div class="text-center">
                    <input type="submit" value="Editar dirección" class="btn_1 full-width">
                </div>
            </div>
        </form>
        <!--form -->
    </div>
@endsection
@section('scripts')
    <script>
        $('#cp').on('keyup', function (e) {
            var cp = e.target.value;
            if (cp.length === 5) {
                $.get('colonias/' + cp, function (data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#idColonia').append('<option value="' + data[i].id + '">' + data[i].d_asenta + '</option>');
                    }
                })
            }
        })
    </script>
    <script>
        $('.direccion-btn').on('click', function (e) {
            var direccionId = $(this).data('id');
            $.get('direcciones/edit/' + direccionId, function (data) {
                $('#aliasUpdate').val(data.alias);
                $('#calleUpdate').val(data.calle);
                $('#numeroExtUpdate').val(data.numeroExt);
                $('#numeroIntUpdate').val(data.numeroInt);
                $('#idUpdate').val(data.id);
                $('#cpUpdate').val(data.cp);
                $.get('colonias/' + data.cp, function (dataCp) {
                    for (var i = 0; i < dataCp.length; i++) {
                        $('#idColoniaUpdate').append('<option value="' + dataCp[i].id + '">' + dataCp[i].d_asenta + '</option>');
                    }
                    $('#idColoniaUpdate').val(data.idColonia);
                })
            })
        })
    </script>
    <script>
        $('.direccion-btn').on('click', function (e) {
            $('#' + this.id).magnificPopup({
                type: 'inline',
                fixedContentPos: true,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: true,
                midClick: true,
                removalDelay: 300,
                closeMarkup: '<button title="%title%" type="button" class="mfp-close"></button>',
                mainClass: 'my-mfp-zoom-in'
            });
        });
    </script>
@endsection
