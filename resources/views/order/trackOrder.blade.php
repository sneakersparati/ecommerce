@extends('index')
@section('styles')
    <link href="{{asset('css/error_track.css')}}" rel="stylesheet">
@endsection
@section('classBody')
    class="bg_gray"
@endsection
@section('content')
    <div id="track_order">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-xl-7 col-lg-9">
                    <img src="img/track_order.svg" alt="" class="img-fluid add_bottom_15" width="200" height="177">
                    <p>Rastrea tu pedido</p>
                    <form>
                        <div class="search_bar">
                            <input type="text" class="form-control" placeholder="Numero de folio">
                            <input type="submit" value="Search">
                        </div>
                    </form>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /track_order -->

    <div class="bg_white">
        <div class="container margin_60_35">
            <div class="main_title">
                <h2>Recientes</h2>
                <span>Productos</span>
            </div>
            <div class="owl-carousel owl-theme products_carousel">
                @foreach($productos as $producto)
                    <div class="item">
                        <div class="grid_item">
                            <span class="ribbon new">Nuevo</span>
                            <figure>
                                <a href="{{route('product.detail',$producto->link)}}">
                                    <img class="owl-lazy" src="img/products/product_placeholder_square_medium.jpg"
                                         data-src="{{$producto->url}}" alt="">
                                </a>
                            </figure>
                            {{--<div class="rating"><i class="icon-star voted"></i><i class="icon-star voted"></i><i--}}
                            {{--class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star"></i></div>--}}
                            <a href="{{route('product.detail',$producto->link)}}">
                                <h3>{{$producto->titulo}}</h3>
                            </a>
                            <div class="price_box">
                                <span class="new_price">{{$producto->formatoPrecio()}}</span>
                            </div>
                            <ul>
                                <li><a href="#0" class="tooltip-1" data-toggle="tooltip" data-placement="left"
                                       title="Añadir a favoritos"><i class="ti-heart"></i><span>Añadir a favoritos</span></a>
                                </li>
                                <li>
                                    <form action="{{route('cart.store')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$producto->id}}">
                                        <input type="hidden" name="titulo" value="{{$producto->titulo}}">
                                        <input type="hidden" name="precio" value="{{$producto->precio}}">
                                        <button class="tooltip-1" data-toggle="tooltip" data-placement="left"
                                                title="Añadir a carrito" type="submit">
                                            <i class="ti-shopping-cart"></i>
                                            <span>Añadir a carrito</span>
                                        </button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                        <!-- /grid_item -->
                    </div>
                @endforeach
            </div>
            <!-- /products_carousel -->
        </div>

        <!-- /container -->
    </div>
@endsection
