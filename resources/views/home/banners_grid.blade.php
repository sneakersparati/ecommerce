<section class="features-area section_gap">
    <div class="container">
        <div class="row features-inner">
            <!-- single features -->
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-features">
                    <div class="f-icon">
                        <img src="img/icons-grid/f-icon1.png" alt="">
                    </div>
                    <h6>Envio gratis</h6>
                    <p>Envio gratuito en pedidos mayores de $700</p>
                </div>
            </div>
            <!-- single features -->
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-features">
                    <div class="f-icon">
                        <img src="img/icons-grid/f-icon2.png" alt="">
                    </div>
                    <h6>Pago Seguro</h6>
                    <p>Tus compras son 100% seguras</p>
                </div>
            </div>
            <!-- single features -->
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-features">
                    <div class="f-icon">
                        <img src="img/icons-grid/f-icon4.png" alt="" width="38">
                    </div>
                    <h6>Formas de pago</h6>
                    <p>Aceptamos tarjetas de crédito, débito, Paypal y transferencias.</p>
                </div>
            </div>
            <!-- single features -->
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-features">
                    <div class="f-icon">
                        <img src="img/icons-grid/f-icon3.png" alt="">
                    </div>
                    <h6>¿Necesitas ayuda?</h6>
                    <a href="tel://5530575652"><p>55-30-57-56-52</p></a>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="top_catagory_areas clearfix mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <!-- Single Catagory -->
            <div class="col-12 col-sm-6 col-md-4 zoom">
                <div class="single_catagory_area d-flex align-items-center justify-content-center bg-img-category"
                     style="background-image: url(img/category-gray/yeezy-category.jpg);"
                     onclick='window.location.href="/product/collection/marca/8"'>
                    <div class="catagory-content">
                        <a href="{{route('product.collection.idMarca',8)}}">YEEZY</a>
                    </div>
                </div>
            </div>
            <!-- Single Catagory -->
            <div class="col-12 col-sm-6 col-md-4 zoom">
                <div class="single_catagory_area d-flex align-items-center justify-content-center bg-img-category"
                     style="background-image: url(img/category-gray/jordan-category.jpg);"
                     onclick='window.location.href="/product/collection/marca/3"'>
                    <div class="catagory-content">
                        <a href="{{route('product.collection.idMarca',3)}}">JORDAN</a>
                    </div>
                </div>
            </div>
            <!-- Single Catagory -->

            <div class="col-12 col-sm-6 col-md-4 zoom">
                <a href="{{route('product.collection.idMarca',7)}}">
                    <div class="single_catagory_area d-flex align-items-center justify-content-center bg-img-category"
                         style="background-image: url(img/category-gray/off-white-category.jpg);"
                         onclick='window.location.href="/product/collection/marca/7"'>
                        <div class="catagory-content">
                            <a href="{{route('product.collection.idMarca',7)}}">OFF WHITE</a>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

