<div class="container margin_60_35">
    <div class="main_title">
        <h2>Nuevos</h2>
        <span>Productos</span>
    </div>
    <div class="owl-carousel owl-theme products_carousel">
        @foreach($productos as $producto)
            <div class="item">
                <div class="grid_item product__item">
                    <span class="ribbon new">Nuevo</span>
                    <figure class="product__item__pic set-bg">
                        <a href="{{route('product.detail',$producto->link)}}">
                            <img class="owl-lazy" src="img/products/product_placeholder_square_medium.jpg"
                                 data-src="{{$producto->url}}" alt="">
                        </a>
                    </figure>
                    {{--<div class="rating"><i class="icon-star voted"></i><i class="icon-star voted"></i><i--}}
                    {{--class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star"></i></div>--}}
                    <div class="product__item__text">
                        <h6 class="text-left">{{$producto->titulo}}</h6>
                        <a href="{{route('product.detail',$producto->link)}}" class="add-cart">+ Ver detalle</a>
                        <h5 class="text-left">{{$producto->formatoPrecio()}}</h5>
                    </div>
                    <ul>
                        <li>
                            <button class="tooltip-1" data-toggle="tooltip" data-placement="left"
                                    title="Añadir a favoritos" onclick="favoritos({{$producto->id}})"><i
                                        class="ti-heart"></i><span>Añadir a favoritos</span>
                            </button>
                        </li>
                        <li>
                            <form action="{{route('cart.store')}}" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="{{$producto->id}}">
                                <input type="hidden" name="titulo" value="{{$producto->titulo}}">
                                <input type="hidden" name="precio" value="{{$producto->precio}}">
                                <input type="hidden" name="talla" value="26">
                                <button class="tooltip-1" data-toggle="tooltip" data-placement="left"
                                        title="Añadir a carrito" type="submit">
                                    <i class="ti-shopping-cart"></i>
                                    <span>Añadir a carrito</span>
                                </button>
                            </form>
                        </li>
                    </ul>
                </div>
                <!-- /grid_item -->
            </div>
        @endforeach
    </div>
    <!-- /products_carousel -->
</div>
