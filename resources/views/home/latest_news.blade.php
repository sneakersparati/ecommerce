<div class="container margin_60_35">
    <div class="main_title">
        <h2>Ultimas noticias</h2>
        <span>Blog</span>
        <p>Enterate de lo ultimo del mundo de los Sneakers</p>
    </div>
    <div class="row">
        @foreach($blogs as $blog)
            <div class="col-lg-6">
                <a class="box_news" href="{{route('blog.find',$blog->slug)}}">
                    <figure>
                        <img src="{{asset($blog->portada)}}" data-src="{{asset($blog->portada)}}" alt="" width="400"
                             height="266" class="lazy">
                        <figcaption><strong>28</strong>Dec</figcaption>
                    </figure>
                    <ul>
                        <li>by {{$blog->autor}}</li>
                        <li>{{$blog->created_at}}</li>
                    </ul>
                    <h4>{{$blog->titulo}}</h4>
                    <p>{{Str::limit($blog->cuerpo, 40)}}</p>
                </a>
            </div>
        @endforeach
    </div>
    <!-- /row -->
</div>
