<div class="container slider-area" id="sliderDesktop">
    <div data-fadeshow="quick-nav prev-next-nav slide-counter autoplay">
        <!-- Radio -->
        <input type="radio" name="css-fadeshow" id="fs-slide-1"/>
        <input type="radio" name="css-fadeshow" id="fs-slide-2"/>
        <input type="radio" name="css-fadeshow" id="fs-slide-3"/>
        <!-- Slides -->
        <div class="fs-slides">
            <div class="fs-slide">
                <div class="fs-slide-bg img-fluid"
                     style="background-image: url(img/bannerDiaMuertos/bannerweb2px.jpg);"></div>
            </div>
            <div class="fs-slide">
                <div class="fs-slide-bg img-fluid"
                     style="background-image: url(img/bannerDiaMuertos/bannerweb3px.jpg);"></div>
            </div>
            <div class="fs-slide">
                <div class="fs-slide-bg img-fluid"
                     style="background-image: url(img/bannerDiaMuertos/bannerweb1px.jpg);"></div>
            </div>
        </div>

        <!-- Quick Navigation -->
        <div class="fs-quick-nav">
            <label class="fs-quick-btn" for="fs-slide-1"></label>
            <label class="fs-quick-btn" for="fs-slide-2"></label>
            <label class="fs-quick-btn" for="fs-slide-3"></label>
        </div>

        <!-- Prev Navigation -->
        <div class="fs-prev-nav">
            <label class="fs-prev-btn" for="fs-slide-1"></label>
            <label class="fs-prev-btn" for="fs-slide-2"></label>
            <label class="fs-prev-btn" for="fs-slide-3"></label>
        </div>

        <!-- Next Navigation -->
        <div class="fs-next-nav">
            <label class="fs-next-btn" for="fs-slide-1"></label>
            <label class="fs-next-btn" for="fs-slide-2"></label>
            <label class="fs-next-btn" for="fs-slide-3"></label>
        </div>

        <!-- Slide Counter (only one required) -->
        <div class="fs-slide-counter">
            <span class="fs-slide-counter-current"></span>/<span class="fs-slide-counter-total"></span>
        </div>

    </div>
</div>

<div class="container slider-area" id="sliderMobile">
    <div data-fadeshow="quick-nav prev-next-nav slide-counter autoplay">
        <!-- Radio -->
        <input type="radio" name="css-fadeshow" id="fs-slide-1x"/>
        <input type="radio" name="css-fadeshow" id="fs-slide-2x"/>
        <input type="radio" name="css-fadeshow" id="fs-slide-3x"/>
        <!-- Slides -->
        <div class="fs-slides">
            <div class="fs-slide">
                <div class="fs-slide-bg img-fluid"
                     style="background-image: url(img/bannerDiaMuertos/BannerResponse2.jpg);"></div>
            </div>
            <div class="fs-slide">
                <div class="fs-slide-bg img-fluid"
                     style="background-image: url(img/bannerDiaMuertos/BannerResponse3.jpg);"></div>
            </div>
            <div class="fs-slide">
                <div class="fs-slide-bg img-fluid"
                     style="background-image: url(img/bannerDiaMuertos/BannerResponse1.jpg);"></div>
            </div>
        </div>

        <!-- Quick Navigation -->
        <div class="fs-quick-nav">
            <label class="fs-quick-btn" for="fs-slide-1x"></label>
            <label class="fs-quick-btn" for="fs-slide-2x"></label>
            <label class="fs-quick-btn" for="fs-slide-3x"></label>
        </div>

        <!-- Prev Navigation -->
        <div class="fs-prev-nav">
            <label class="fs-prev-btn" for="fs-slide-1x"></label>
            <label class="fs-prev-btn" for="fs-slide-2x"></label>
            <label class="fs-prev-btn" for="fs-slide-3x"></label>
        </div>

        <!-- Next Navigation -->
        <div class="fs-next-nav">
            <label class="fs-next-btn" for="fs-slide-1x"></label>
            <label class="fs-next-btn" for="fs-slide-2x"></label>
            <label class="fs-next-btn" for="fs-slide-3x"></label>
        </div>

        <!-- Slide Counter (only one required) -->
        <div class="fs-slide-counter">
            <span class="fs-slide-counter-current"></span>/<span class="fs-slide-counter-total"></span>
        </div>

    </div>
</div>

