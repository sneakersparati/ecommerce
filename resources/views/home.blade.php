@extends('index')
@section('content')
    @include('home.carousel')
    @include('home.banners_grid')
    @include('home.top_selling')
    @include('home.parallax')
    @include('home.featured')
    {{--@include('home.brands')--}}
    @include('home.latest_news')
@endsection
@section('scripts')
    <script src="{{asset('js/carousel-home.min.js')}}"></script>
    <script>
        function favoritos(id) {
            var miCookie = readCookie("favoritos");
            if (miCookie == null) {
                var arreglo = [];
                arreglo.push(id);
                document.cookie = "favoritos=" + JSON.stringify(arreglo);
            } else {
                var arreglo1 = JSON.parse(miCookie);
                arreglo1.push(id);
                document.cookie = "favoritos=" + JSON.stringify(arreglo1);
            }

        }

        function readCookie(name) {
            return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + name.replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
        }
    </script>
@endsection
