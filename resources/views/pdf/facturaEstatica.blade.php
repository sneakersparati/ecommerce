<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sneaker Store</title>
</head>
<style>
    .container {
        text-align: center
    }

    .left {
        float: left;
    }

    .right {
        float: right;
    }

    .datos {
        padding-top: 5em;
    }

    .center {
        padding-left: 20em;
    }

    .datos-factura {
        padding-top: 5em;
    }

    .datos-factura table {
        width: 100%;
        borden: solid 1px;
    }

    .datos-factura table thead {
        background: #E9E5E5;
        text-align: center;
    }

    .datos-factura thead th {
        padding: .3em;
    }

    .datos-factura tbody {
        text-align: center;
    }

    .datos-productos {
        padding-top: 2em;
    }

    .datos-productos table {
        width: 100%;
        borden: solid 1px;
        borden-collapse: collapse;
    }

    .datos-productos table thead {
        background: #E9E5E5;
        text-align: center;
    }

    .datos-productos thead th {
        padding: .3em;
    }

    .datos-productos tbody {
        text-align: center;
    }

    .datos-productos tbody td {
        padding: .2em;
        borden-bottom: solid 1px;
    }

    .datos-total {
        padding-top: 2em;
    }

    .datos-total table {
        borden: solid 1px;
    }

    .datos-total table th {
        background: #E9E5E5;
        padding: .5em .5em .5em 1.5em;
    }

    .datos-total table td {
        padding-left: 4em;
        padding-right: .5em;
    }


</style>
<body>
<div class="container">
    <div class="right">
        27/08/2020<br>

    </div>
    <div class="left">
        <img src="{{asset('img/logo_pagina.png')}}" alt="logo" style="width: 100px;">
    </div>
</div>
<div class="datos">
    <div class="left">
        <b>Dirección de envío</b> <br> <br>
        Calle hacienda los reyes 103-B<br>
        Real de haciendas <br>
        20196 <br>
        Aguascalientes, Aguascalientes <br>

    </div>
    <div class="center">
        <b>Datos de cliente</b> <br> <br>
        Carlos Emilio Chavez Barrientos <br>
        charlychavezchingon@gmail.com <br>
        4495843881 <br>
    </div>
</div>
<div class="datos-factura">
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th>Fecha de compra</th>
            <th>Folio de la órden</th>
            <th>Forma de pago</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>26/08/2020</td>
            <td>SPTE-026002</td>
            <td>Efectivo</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="datos-productos">
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th>Imagen</th>
            <th>Descripcion</th>
            <th>Precio</th>
            <th>Talla</th>
            <th>Cantidad</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><img src="https://images-sneakes.s3.us-east-2.amazonaws.com/products/1039.jpg" alt=""
                     style="width: 50px"></td>
            <td>AIR JORDAN 4 RETRO METALLIC PACK - TOTAL ORANGE</td>
            <td>$5,429.00</td>
            <td>25</td>
            <td>1</td>
            <td>$5,429.00</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="datos-total">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th>Subtotal:</th>
            <td>$5,429.00}</td>
        </tr>
        <tr>
            <th>Gastos de envio:</th>
            <td>$0.00</td>
        </tr>
        <tr>
            <th>Total:</th>
            <td><b>$5,429.00</b></td>
        </tr>
    </table>
</div>
</body>
</html>

