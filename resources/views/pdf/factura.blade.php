<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sneaker Store</title>
</head>
<style>
    .container {
        text-align: center
    }

    .left {
        float: left;
    }

    .right {
        float: right;
    }

    .datos {
        padding-top: 5em;
    }

    .center {
        padding-left: 20em;
    }

    .datos-factura {
        padding-top: 5em;
    }

    .datos-factura table {
        width: 100%;
        borden: solid 1px;
    }

    .datos-factura table thead {
        background: #E9E5E5;
        text-align: center;
    }

    .datos-factura thead th {
        padding: .3em;
    }

    .datos-factura tbody {
        text-align: center;
    }

    .datos-productos {
        padding-top: 2em;
    }

    .datos-productos table {
        width: 100%;
        borden: solid 1px;
        borden-collapse: collapse;
    }

    .datos-productos table thead {
        background: #E9E5E5;
        text-align: center;
    }

    .datos-productos thead th {
        padding: .3em;
    }

    .datos-productos tbody {
        text-align: center;
    }

    .datos-productos tbody td {
        padding: .2em;
        borden-bottom: solid 1px;
    }

    .datos-total {
        padding-top: 2em;
    }

    .datos-total table {
        borden: solid 1px;
    }

    .datos-total table th {
        background: #E9E5E5;
        padding: .5em .5em .5em 1.5em;
    }

    .datos-total table td {
        padding-left: 4em;
        padding-right: .5em;
    }


</style>
<body>
<div class="container">
    {{--<div class="left">--}}
    {{--<img src="{{App\Helpers\asset('images/logo_sneakers.png')}}" alt="">--}}
    {{--</div>--}}
    <div class="right">
        <?php
        $date = new DateTime($orden->created_at);
        echo $date->format('Y-m-d');
        ?><br>

    </div>
    <div class="left">
        <img src="{{asset('img/logo_pagina.png')}}" alt="logo" style="width: 100px;">
    </div>
</div>
<div class="datos">
    <div class="left">
        <b>Dirección de envío</b> <br> <br>
        {{$orden->calleFactura." ".$orden->numeroExtFactura."".$orden->numeroIntFactura}} <br>
        {{$orden->coloniaFactura}} <br>
        {{$orden->cpFactura." ".$orden->municipioFactura}} <br>
        {{$orden->estadoFactura}} <br>

    </div>
    <div class="center">
        <b>Datos de cliente</b> <br> <br>
        {{$orden->nombreFactura." ".$orden->apellidosFactura}} <br>
        {{$orden->emailFactura}} <br>
        {{$orden->telefonoFactura}}
    </div>
</div>
<div class="datos-factura">
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th>Fecha de compra</th>
            <th>Folio de la órden</th>
            <th>Forma de pago</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$orden->created_at}}</td>
            <td>{{$orden->numeroFolio}}</td>
            <td>{{$orden->metodoPago}}</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="datos-productos">
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th>Imagen</th>
            <th>Descripcion</th>
            <th>Precio</th>
            <th>Talla</th>
            <th>Cantidad</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach($productos as $producto)
            <tr>
                <td><img src="{{json_decode($producto->imagenes, true)[0]}}" alt="" style="width: 50px"></td>
                <td>{{$producto->descripcion}}</td>
                <td>${{number_format($producto->precioVenta,2)}}</td>
                <td>{{$producto->talla}}</td>
                <td>{{$producto->cantidad}}</td>
                <td>${{number_format($producto->precioVenta * $producto->cantidad,2)}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="datos-total">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th>Subtotal:</th>
            <td>${{number_format($orden->total, 2)}}</td>
        </tr>
        <tr>
            <th>Gastos de envio:</th>
            <td>${{number_format($orden->costoEnvio,2)}}</td>
        </tr>
        <tr>
            <th>Total:</th>
            <td><b>${{number_format($orden->total + $orden->costoEnvio,2)}}</b></td>
        </tr>
    </table>
</div>
</body>
</html>

