<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('product/detail/{lang}', 'ProductController@detail')->name('product.detail');
Route::get('product/collection/marca/{idMarca}', 'ProductController@collectionIdMarca')->name('product.collection.idMarca');
Route::get('product/collection/categoria/{idCategoria}', 'ProductController@collectionIdCategoria')->name('product.collection.categoria');
Route::get('product/collection/{idCategoria}/{idMarca}', 'ProductController@collection')->name('product.collection');
Route::get('product/search/{query}', 'ProductController@search')->name('product.search');
//Route::get('product/slug', 'ProductController@slug')->name('product.slug');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('cart', 'CartController@index')->name('cart.index');
Route::post('cart', 'CartController@store')->name('cart.store');
Route::post('cart/login', 'LoginCartController@login')->name('cart.login');
Route::put('cart/destroy', 'CartController@destroy')->name('cart.destroy');
Route::patch('cart/update/{id}', 'CartController@update')->name('cart.update');
Route::get('checkout', 'CheckoutController@index')->name('checkout.index');
Route::post('checkout', 'CheckoutController@store')->name('checkout.store');
Route::get('about', 'PaginasInteres\AboutController@index')->name('about.index');
Route::get('contact', 'PaginasInteres\ContactController@index')->name('contact.index');
Route::get('faq', 'PaginasInteres\FaqController@index')->name('faq.index');
Route::get('shipingPolicy', 'PaginasInteres\ShipingPolicyController@index')->name('shipingPolicy.index');
Route::get('terms', 'PaginasInteres\TermsController@index')->name('terms.index');
//Route::post('payments/stripe', 'PaymentController@stripePayment')->name('payment.stripe');
Route::post('payments/setSecret', 'PaymentController@setSecret')->name('payments.setSecret');
Route::get('colonias/{cp}', 'CpSepomexController@getColonias')->name('colonias.get');
Route::post('user/login', 'UserController@login')->name('user.login');
Route::post('user/logout', 'UserController@logout')->name('user.logout');
Route::get('direcciones', 'DireccionesController@index')->name('direcciones.index')->middleware('auth');
Route::get('direcciones/button', 'DireccionesController@button')->name('direcciones.button')->middleware('auth');
Route::post('direcciones', 'DireccionesController@store')->name('direcciones.store')->middleware('auth');
Route::get('direcciones/edit/{id}', 'DireccionesController@edit')->name('direcciones.edit')->middleware('auth');
Route::post('direcciones/update', 'DireccionesController@update')->name('direcciones.update')->middleware('auth');
Route::post('direcciones/destroy', 'DireccionesController@destroy')->name('direcciones.destroy')->middleware('auth');
Route::get('perfil', 'UserController@perfil')->name('user.perfil')->middleware('auth');
Route::post('perfil/update/{id}', 'UserController@perfilUpdate')->name('user.perfilUpdate')->middleware('auth');
Route::get('blog', 'BlogController@index')->name('blog.index');
Route::get('blog-post/{slug}', 'BlogController@find')->name('blog.find');
Route::post('blog-comment', 'BlogController@comentSave')->name('blog.comentSave');
Route::get('track-order', 'OrderController@trackOrderIndex')->name('trackOrder.index');
Route::post('track-order', 'OrderController@trackOrderFind')->name('trackOrder.find');
Route::post('newsletter', 'NewsLetterController@store')->name('newsletter.store');
Route::get('factura-estatica', 'CheckoutController@facturaEstatica')->name('factura-estatica');
Route::get('atributos', 'ProductController@setTallas')->name('atributos');
Route::post('contactanos', 'PaginasInteres\ContactController@save')->name('contactanos.save');
Route::get('mis-ordenes', 'OrderController@misOrdenes')->name('orders.misOrdenes')->middleware('auth');
Route::get('detalle-orden/{id}', 'OrderController@detalleOrden')->name('orders.detalleOrden')->middleware('auth');
Route::get('email-prueba', function () {
    try {
        $email = 'ccedillomoller@gmail.com';
        $template_data = ['name' => 'Carlos'];
        Mail::send(['html' => 'email.test'], $template_data,
            function ($message) use ($email) {
                $message->to($email)
                    ->from('ventas@sneakersparati.com.mx')//not sure why I have to add this
                    ->subject('Gracias por tu registro');
            });

        return Response::json(['code' => 200, 'msg' => 'Sent successfully']);

    } catch (Exception $ex) {
        return Response::json(['code' => 200, 'msg' => 'Something went wrong, please try later.']);
    }
});
Route::post('wishlist/add', 'WishController@add')->name('wishlist.add');
Route::get('wishlist', 'WishController@show')->name('wishlist.show');
